<?php

namespace App\Http\Controllers;

use App\Interfaces\StudentRepositoryInterface;
use App\Models\Student;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    use RelationTraits;
    protected StudentRepositoryInterface $studentRepository;
    public function __construct(StudentRepositoryInterface $studentRepository) {
        $this->studentRepository = $studentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tutions = DB::table('tutions')->pluck('name', 'id');
        $tution = $tutions->toArray();
        return view('students.create',compact('tution'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $tution = $request->input('tutions');
        // $student = Student::create($request->all());
        // $student->tutions()->attach($tution);
        $students = $this->studentRepository->create($request);
        return redirect()->route('students.index');


        //
        // $tution = $request->input('products');
        // $student = Student::create($request->all());
        // $student->tutions()->attach($tution);

        // return redirect()->route('students.index');

        // $product = $request->input('products');
        // $store = Store::create($request->all());
        // $store->products()->attach($product);
        // return redirect()->route('stores.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $student = Student::find($id);
        $tutions = $student->tutions;
        return view('students.show', compact('student','tutions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $student = Student::find($id);
        $tutions = DB::table('tutions')->pluck('name', 'id');
        $tution = $tutions->toArray();
        if (is_null($student && $tution)) {
            return redirect()->back();
        } else {
            return view('students.edit', compact('student', 'tution'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        // $student = Student::find($id);
        // $student->update($request->all());
        // $tution = $request->input('tutions');
        // $student->tutions()->attach($tution);
        // $this->attachTution($request, $id);
        // $this->studentRepository->update($id,$request);
        $students = $this->studentRepository->update($id,$request);
        return redirect()->route('students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Student::find($id)->delete();
        $students = $this->studentRepository->delete($id);

        return redirect()->route('students.index');
    }
    public function fetchStudents(Request $request)
    {

        return $this->studentRepository->fetchStudents($request);

    }
    public function detachproduct(Request $request)
    {
        $request = request()->all();
        $product_id=$request['product_Id'];
        $store_id=$request['store_Id'];
        $model=$request['model'];
        $function_name=$request['function_name'];

        // $product = $model::find($product_id);
        // $product->$function_name->detach($store_id);

        // $data=[
        //     'store_id'=>$store_id,
        //     'product_id'=>$product_id,
        // ];
        return response()->json(['message'=>'success']);
        // return response()->json(['store_id'=>$store_id,'product_id'=>$product_id,]);
    }
}
