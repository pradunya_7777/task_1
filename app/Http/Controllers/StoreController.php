<?php

namespace App\Http\Controllers;

use App\Interfaces\StoreRepositoryInterface;
use App\Models\Product;
use App\Models\Store;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    use RelationTraits;
    protected StoreRepositoryInterface $storeRepository;
    public function __construct(StoreRepositoryInterface $storeRepository) {
        $this->storeRepository = $storeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('stores.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $product =DB::table('products')->pluck('name','id');
        $products = $product->toArray();
        // dd($store);
        return view('stores.create',compact('products'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // $product = $request->input('products');
        // $store = Store::create($request->all());
        // $store->products()->attach($product);
        $stores = $this->storeRepository->create($request);

        return redirect()->route('stores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::find($id);
        $products = $store->products;
        return view('stores.show', compact('store','products'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product =DB::table('products')->pluck('name','id');
        $products = $product->toArray();
        $store=Store::find($id);
        if (is_null($store)) {
            return redirect()->back();
        } else {
            return view('stores.edit', compact('store','products'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = $this->storeRepository->update($id, $request);

        if (!$store) {
            return redirect()->route('stores.index');
        }

        $this->attachStore($request, $store);


        return redirect()->route('stores.index');


        // $store = Store::find($id);
        // $store->update($request->all());
        // $product = $request->input('products');
        // $store->products()->attach($product);

        // $products = $this->storeRepository->create($request, $id);

        // $this->attachProduct($request,$id);
        // return redirect()->route('stores.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Store::find($id)->delete();
        $stores = $this->storeRepository->delete($id);

        return redirect()->route('stores.index');
    }
    public function fetchStores(Request $request)
    {
        //

        return $this->storeRepository->fetchStores($request);
    }

}
