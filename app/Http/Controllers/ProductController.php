<?php

namespace App\Http\Controllers;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use App\Models\Student;
use App\Models\Store;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ProductController extends Controller
{
    use RelationTraits;
    protected ProductRepositoryInterface $productRepository;
    public function __construct(ProductRepositoryInterface $productRepository) {
        $this->productRepository = $productRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('products.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $store =DB::table('stores')->pluck('name','id');
        $stores = $store->toArray();
        // $stores = Store::select('id','name')->get()->pluck('name','id');
        // dd($stores);
        return view('products.create',compact('stores'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $store = $request->input('stores');
        // $product = Product::create($request->all());
        // $product->stores()->attach($store);
        $products = $this->productRepository->create($request);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Product::find($id);
        $stores = $product->stores;
        // dd($stores->toArray());
        return view('products.show', compact('product','stores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::find($id);
        $moduleName = "Product";
        if (is_null($product)) {
            return redirect()->back();
        } else {
            return view('products.edit', compact('product','moduleName'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $products = $this->productRepository->update($id,$request);
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Product::find($id)->delete();
        $products = $this->productRepository->delete($id);

        return redirect()->route('products.index');
    }

    public function fetchProducts(Request $request)
    {
        //

        return $this->productRepository->fetchProducts($request);
    }

    public function detachproduct (Request $request)
    {
        $request = request()->all();
        $parent_Id=$request['parent_Id'];
        $child_Id=$request['child_Id'];
        $Model=$request['model'];
        $Function_Name=$request['function_name'];
        $modelClass = 'App\\Models\\' . $Model;
        $detach = $modelClass::find($parent_Id);
        $detach->$Function_Name()->detach($child_Id);

        // $data=[
        //     'child_Id'=>$child_Id,
        //     'parent_Id'=>$parent_Id,
        //     'model'=>$Model,
        //     'function_name'=>$Function_Name
        // ];
        return response()->json($request);
        // return response()->json(['child_Id'=>$child_Id,'parent_Id'=>$parent_Id,]);

    }
}
