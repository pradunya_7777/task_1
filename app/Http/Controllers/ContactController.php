<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactsRequest;
use App\Interfaces\ContactRepositoryInterface;
use App\Models\Account;
use App\Models\Contact;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    use RelationTraits;
    protected ContactRepositoryInterface $contactRepository;
    public function __construct(ContactRepositoryInterface $contactRepository) {
        $this->contactRepository = $contactRepository;
    }

    public function create()
    {
        // $accounts=Account::all('id','name');
        $accounts = DB::table('accounts')->pluck('name', 'id');
        $account = $accounts->toArray();
        // dd($account);
        return view('contacts.create', compact('account'));
    }

    public function store(StoreContactsRequest $request)
    {
        // Contact::create($request->all());
        $contacts = $this->contactRepository->create($request);
        return redirect()->route('contacts.view');
    }
    public function view()
    {
        // $contacts = Contact::paginate(10);
        return view('contacts.index');
    }
    public function show($id)
    {
        $contact = Contact::find($id);
        return view('contacts.show', compact('contact'));
    }
    public function edit($id, Request $request)
    {
        $contact = Contact::find($id);
        $accounts = DB::table('accounts')->pluck('name', 'id');
        $account = $accounts->toArray();
        if (is_null($contact && $account)) {
            return redirect()->back();
        } else {
            return view('contacts.edit', compact('contact', 'account'));
        }
    }

    public function update(StoreContactsRequest $request, $id)
    {
        // $contact = Contact::find($id);
        // $contact->update($request->all());
        $contact = $this->contactRepository->update($id,$request);

        return redirect()->route('contacts.view');

        // return redirect()->route('contacts.view');
    }
    public function delete($id)
    {
        // Contact::find($id)->delete();
        $contacts = $this->contactRepository->delete($id);

        return redirect()->route('contacts.view');
    }

    public function fetchContacts(Request $request)
    {

        return $this->contactRepository->fetchContacts($request);
        // return view('contacts.fetch', ['contacts' => $contacts]);
    }
}
