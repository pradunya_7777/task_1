<?php

namespace App\Http\Controllers;

use App\Interfaces\TutionRepositoryInterface;
use App\Models\Tution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutionController extends Controller
{
    protected TutionRepositoryInterface $tutionRepository;
    public function __construct(TutionRepositoryInterface $productRepository) {
        $this->tutionRepository = $productRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('tutions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tutions.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Tution::create($request->all());
        $tutions = $this->tutionRepository->create($request);

        return redirect()->route('tutions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tution = Tution::find($id);
        return view('tutions.show', compact('tution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tution = Tution::find($id);
        $students = DB::table('students')->pluck('name', 'id');
        $student = $students->toArray();
        if (is_null($student && $tution)) {
            return redirect()->back();
        } else {
            return view('tutions.edit', compact('student', 'tution'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tution = Tution::find($id);
        $tution->update($request->all());
        $student = $request->input('students');
        $tution->students()->attach($student);
        return redirect()->route('tutions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Tution::find($id)->delete();
        $tutions = $this->tutionRepository->delete($id);

        return redirect()->route('tution.index');
    }
    public function fetchTution(Request $request)
    {

        return $this->tutionRepository->fetchTutions($request);
    }
}
