<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAcitivityRequest;
use App\Interfaces\ActivityRepositoryInterface;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityController extends Controller
{
    protected ActivityRepositoryInterface $activityRepository;
    public function __construct(ActivityRepositoryInterface $activityRepository) {
        $this->activityRepository = $activityRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('activities.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $projects = DB::table('projects')->pluck('name', 'id');
        $project = $projects->toArray();
        return view('activities.create',compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAcitivityRequest $request)
    {
        //
        // Activity::create($request->all());
        // $file = $request->file('file');
        // $filename = $file()->getClientOriginalExtension();
        // $filePath = $file->store('files', 'public');
        // echo $filename.' '.$filePath;

        if ($file = $request->file()) {
            $destinationPath = 'files/';
            $profileImage = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $input[''] = "$profileImage";
            echo $profileImage;
        }

        $contacts = $this->activityRepository->create($request);

        return redirect()->route('activities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $activity = Activity::find($id);
        return view('activities.show', compact('activity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        //
        $activity = Activity::find($id);
        if (is_null($activity)) {
            return redirect()->back();
        } else {
            return view('activities.edit', compact('activity'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAcitivityRequest $request, $id)
    {
        //
        // $activity = Activity::find($id);
        // $activity->update($request->all());
        $contacts = $this->activityRepository->update($id, $request);

        return redirect()->route('activities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Activity::find($id)->delete();
        $contacts = $this->activityRepository->delete($id);

        return redirect()->route('activities.index');
    }

    public function fetchActivities(Request $request)
    {

        return $this->activityRepository->fetchActivities($request);
    }
}
