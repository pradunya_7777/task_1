<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLeadRequest;
use App\Interfaces\LeadRepositoryInterface;
use App\Models\Lead;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeadController extends Controller
{
    use RelationTraits;
    protected LeadRepositoryInterface $leadRepository;
    public function __construct(LeadRepositoryInterface $leadRepository) {
        $this->leadRepository = $leadRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('leads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $accounts =DB::table('accounts')->pluck('name','id');
        $account = $accounts->toArray();
        return view('leads.create',compact('account'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeadRequest $request)
    {
        //
        // Lead::create($request->all());
        $leads = $this->leadRepository->create($request);
        return redirect()->route('leads.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $lead = Lead::find($id);
        return view('leads.show', compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lead = Lead::find($id);
        $accounts = DB::table('accounts')->pluck('name', 'id');
        $account = $accounts->toArray();
        if (is_null($lead && $account)) {
            return redirect()->back();
        } else {
            return view('leads.edit', compact('lead', 'account'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLeadRequest $request, $id)
    {
        //
        // $lead = Lead::find($id);
        // $lead->update($request->all());
        $leads = $this->leadRepository->update($id, $request);

        return redirect()->route('leads.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Lead::find($id)->delete();
        $leads = $this->leadRepository->delete($id);

        return redirect()->route('leads.index');
    }

    public function fetchLeads(Request $request)
    {
        //

        return $this->leadRepository->fetchLeads($request);
    }
}
