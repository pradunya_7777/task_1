<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAccountRequest;
use App\Interfaces\AccountRepositoryInterface;
use App\Models\Account;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    //
    protected AccountRepositoryInterface $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository) {
        $this->accountRepository = $accountRepository;
    }


    public function create()
    {

        return view('accounts.create');
    }

    public function store(StoreAccountRequest $request)
    {
        // $account = Account::create($request->all());
        $accounts = $this->accountRepository->create($request);
        return redirect()->route('accounts.view');
    }
    public function view()
    {
        // $accounts = Account::all();
        // $accounts = $this->accountRepository->all();
        return view('accounts.index');

    }
    public function show($id)
    {
        $account = Account::find($id);
        $contacts = $account->contacts;
        $leads = $account->leads;
        return view('accounts.show', compact('account','contacts','leads'));
    }
    public function edit($id, Request $request)
    {
        $account = Account::find($id);
        if (is_null($account)) {
            return redirect()->back();
        } else {
            return view('accounts.edit', compact('account'));
        }
    }

    public function update(StoreAccountRequest $request, $id)
    {
        // dd($id);
        // $account = Account::find($id);
        // $account->update($request->all());
        $accounts = $this->accountRepository->update($id,$request);

        return redirect()->route('accounts.view');
    }

    public function delete($id)
    {
        // Account::find($id)->delete();
        $accounts = $this->accountRepository->delete($id);
        return redirect()->route('accounts.view');
    }

    public function fetchAccounts(Request $request)
    {
        // dd($request->toArray());
        // $start = $request->all();
        // // dd($start);
        // $accounts = DB::table('accounts')->latest()
        // ->offset($start['start'])
        // ->limit($start['length'])
        // ->get();
        // $acc = Account::all();
        // return response()->json(['recordsTotal' =>count($acc), 'recordsFiltered' => count($acc), 'data' => $accounts]);
      return $this->accountRepository->fetchAccounts($request);
        // dd($accounts);
        // return response()->json($accounts);


    }
}
