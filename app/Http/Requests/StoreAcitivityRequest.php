<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAcitivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'subject'=>'required',
            'start'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'subject.required' => 'A Subject is required',
            'start.required' => 'A Start Position is required'
        ];
    }
}
