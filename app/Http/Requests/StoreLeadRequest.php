<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'phone'=>'min:10',
            'account_id'=>'required',
            'lead_source'=>'required',

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'A Lead Name is required',
            'account_id.required' => 'Account required',
            'phone.min'=>'Phone number required upto 10 digit',
            'lead_source'=>'Lead source is required'
        ];
    }
}
