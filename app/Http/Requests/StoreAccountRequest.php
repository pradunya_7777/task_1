<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|max:255',
            'industry'=>'required',
            'phone'=>'min:10',



        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'A Name is required',
            'industry.required' => 'A Industry is required',
            'phone.min'=>'Phone number required upto 10 digit',
        ];
    }
}
