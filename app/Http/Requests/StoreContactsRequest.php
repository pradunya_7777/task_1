<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:255',
            'customer_position' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'A title is required',
            'customer_position.required' => 'A Customer Position is required',
        ];
    }
}
