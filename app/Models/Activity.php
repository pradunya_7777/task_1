<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Activity extends Model
{
    use HasFactory,HasUuid;
    protected $fillable =['subject','relatedTo','start','end','taskType','status','description','project_id'];

    public $incrementing = false;
    protected $keyType = 'string';

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);

    }
}
