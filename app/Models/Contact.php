<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use App\Traits\RelationTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Model
{
    use HasFactory,HasUuid,RelationTraits;

    protected $fillable = ['name', 'designation', 'email', 'phone', 'customer_type', 'customer_position', 'description', 'comment', 'contact_status', 'joincommunity', 'followupdate', 'account_id'];

    public $incrementing = false;
    protected $keyType = 'string';

    // public function account(): BelongsTo
    // {
    //     return $this->belongsTo(Account::class);

    // }
    public function accounts()
    {
        return $this->belongsToMany(Account::class);
    }
}
