<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use App\Traits\RelationTraits;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Account extends Model
{
    use HasFactory, HasUuid, RelationTraits;

    protected $fillable = ['name', 'email', 'phone', 'partnercode', 'industry', 'website', 'comment_history', 'description_summary'];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }
    public function leads()
    {
        return $this->hasMany(Lead::class);
    }

}
