<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use App\Traits\RelationTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Lead extends Model
{
    use HasFactory, HasUuid, RelationTraits;

    protected $fillable = ['name', 'account_id', 'type', 'status', 'phone', 'email', 'product_category', 'no_of_user', 'industry', 'lead_source'];

    public $incrementing = false;
    protected $keyType = 'string';

    // public function account(): BelongsTo
    // {
    //     return $this->belongsTo(Account::class);
    // }
    public function accounts()
    {
        return $this->belongsToMany(Account::class);
    }
}
