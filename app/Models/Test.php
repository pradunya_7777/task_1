<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Test extends Model
{
    use HasFactory ,HasUuid;

    protected $fillable=['name'];
    protected $keyType = 'string';
    public $incrementing = false;

}
