<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Hash;

class Tution extends Model
{
    use HasFactory, HasUuid;
    protected $fillable = ['name','teacher_instructor','schedule','class_room_location','class_capacity','class_material','start_date','end_date'];


    public $incrementing = false;
    protected $keyType = 'string';

    public function students()
    {
        return $this->belongsToMany(Student::class,'student_tution','tution_id','student_is');
    }
}
