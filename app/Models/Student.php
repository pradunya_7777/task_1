<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use App\Traits\RelationTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Student extends Model
{
    use HasFactory, HasUuid,RelationTraits;
    protected $fillable =['name','gender','dob','contact','guardian_name','guardian_contact','class','year','department','address'];


    public $incrementing = false;
    protected $keyType = 'string';

    public function tutions()
    {
        return $this->belongsToMany(Tution::class,'student_tution','student_id','tution_id');
    }
}

