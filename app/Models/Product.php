<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use App\Traits\DetachTrait;
use App\Traits\RelationTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory,HasUuid, RelationTraits;

    protected $fillable = ['id','name','category','brand','price'];

    public $incrementing = false;
    protected $keyType = 'string';

    public function stores()
    {
        return $this->belongsToMany(Store::class,'product_store', 'product_id', 'store_id');
    }
}
