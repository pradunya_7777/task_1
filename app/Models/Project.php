<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory,HasUuid;
    protected $fillable=['name','implementation_status','priority','project_type','start_date','due_date','product','project_hosting_type'];

    public $incrementing = false;
    protected $keyType = 'string';

    public function activities(): HasMany
    {
        return $this->hasMany(Activity::class);
    }
}
