<?php
namespace App\Interfaces;

use App\Http\Requests\StoreAccountRequest;
use Illuminate\Http\Request;

interface AccountRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchAccounts(Request $request);
}
