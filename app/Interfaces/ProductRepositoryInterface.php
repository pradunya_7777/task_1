<?php
namespace App\Interfaces;

use Illuminate\Http\Request;

interface ProductRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchProducts(Request $request);

}
