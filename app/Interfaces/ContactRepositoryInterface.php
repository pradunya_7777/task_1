<?php
namespace App\Interfaces;

use App\Http\Requests\StoreContactsRequest;
use Illuminate\Http\Request;

interface ContactRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchContacts(Request $request);

}
