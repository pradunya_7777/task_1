<?php
namespace App\Interfaces;

use App\Http\Requests\StoreAcitivityRequest;
use Illuminate\Http\Request;

interface ActivityRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchActivities(Request $request);

}
