<?php
namespace App\Interfaces;

use Illuminate\Http\Request;

interface TutionRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchTutions(Request $request);

}
