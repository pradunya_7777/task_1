<?php
namespace App\Interfaces;

use App\Http\Requests\StoreLeadRequest;
use Illuminate\Http\Request;

interface LeadRepositoryInterface
{
    public function all();
    public function create(Request $request);
    public function update($id, Request $request);
    public function delete($id);
    public function fetchLeads(Request $request);

}
