<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TextInput extends Component
{
    public $name;
    public $label;
    public $value;
    public $attributes=[];
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $label,$value=null,$attributes)
    {
        //
        $this->name=$name;
        $this->label=$label;
        $this->value=$value;
        $this->attributes = $attributes;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        // dd($this->attributes);
        return view('components.text-input');
    }
}
