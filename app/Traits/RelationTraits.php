<?php

namespace App\Traits;

use App\Models\Product;
use Illuminate\Support\Str;

trait RelationTraits
{

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {

            $request = request();
            $requests = request()->all();
            // dump($requests);

            $relationships = $request->only('relation_ship');
            $relationship = json_decode($relationships['relation_ship']);
            // dump($relationships);
            // dump($relationship);
            foreach($relationship as $a => $rel){
                // dump($rel,$a);
                // dump($a);
            foreach ($rel as $key => $relation) {
                // $key = $key . '()';
                // dump($key);
                // dump($relation);
                $relate = $relation->relationshipName;
                // dump($relate);
                if (array_key_exists($relate, $requests)) {
                    $valueFromRequests = $requests[$relate];

                    //Covert to array if it's in string
                    if (!is_array($valueFromRequests)){
                        $valueFromRequests = [$valueFromRequests];
                    }

                    // dump($valueFromRequests);
                    // dump($valueFromRequests);
                    // dd($model->$key);
                    // dd($key,$valueFromRequests);
                    // ['id'=>Str::uuid(), 'product_id'=>$model->id,'store_id'=>"" ]
                    // dump([$valueFromRequests,'id'=> Str::uuid()]);
                    foreach($valueFromRequests as $item)
                    {
                        // dd($item);
                        $attach =  [$item=>['id'=>Str::uuid()]];
                        $model->$key()->attach( $attach);
                        // dd($model->$key()->attach( $attach));
                    }

                }
            }
        }
        // dd('stop');
        });


    }





    // public function detachProduct($request,$id)
    // {

    //     $product = Product::find($id);
    //     $product->update($request->all());
    //     $store = $request->input('stores');
    //     $product->stores()->attach($store);
    // }
    // public function attachTution($request, $id){

    //     $student = Student::find($id);
    //     $student->update($request->all());
    //     $tution = $request->input('tutions');
    //     $student->tutions()->attach($tution);
    // }
}
