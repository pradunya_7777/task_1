<?php

namespace App\Providers;

use App\Interfaces\AccountRepositoryInterface;
use App\Interfaces\ContactRepositoryInterface;
use App\Interfaces\ActivityRepositoryInterface;
use App\Interfaces\LeadRepositoryInterface;
use App\Interfaces\ProductRepositoryInterface;
use App\Interfaces\ProjectRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\StudentRepositoryInterface;
use App\Interfaces\TutionRepositoryInterface;
use App\Repositories\AccountRepository;
use App\Repositories\ActivityRepository;
use App\Repositories\ContactRepository;
use App\Repositories\LeadRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\StoreRepository;
use App\Repositories\StudentRepository;
use App\Repositories\TutionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(AccountRepositoryInterface::class, AccountRepository::class);
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
        $this->app->bind(ActivityRepositoryInterface::class, ActivityRepository::class);
        $this->app->bind(LeadRepositoryInterface::class, LeadRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(ProjectRepositoryInterface::class, ProjectRepository::class);
        $this->app->bind(StoreRepositoryInterface::class, StoreRepository::class);
        $this->app->bind(StudentRepositoryInterface::class, StudentRepository::class);
        $this->app->bind(TutionRepositoryInterface::class, TutionRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
