<?php

namespace App\Repositories;

use App\Interfaces\StudentRepositoryInterface;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentRepository implements StudentRepositoryInterface{

    public function all()
    {
        return Student::all();
    }
    public function create(Request $request)
    {
        $tution = $request->input('tutions');
        return Student::create($request->all())->tutions()->attach($tution);
        // $product->stores()->attach($store);
        // return Product::create($request->all());
    }
    public function update($id, Request $request)
    {
        // $tution = $request->input('tutions');
        $tution = Student::find($id);
        $tution->update($request->all());
        // $student = Student::find($id)->update($request->all())->tutions()->attach($tution);    for update & attach
        // $product = Product::find($id);
        // $product->update($request->all());
        // return Contact::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Student::find($id)->delete();
    }
    public function fetchStudents(Request $request){
        $start = $request->all();
        $query = DB::table('students');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }

        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $students=$query->offset($start['start'])
        ->limit($start['length'])
        ->get();
        $totalRecord = Student::count();
        return response()->json(['recordsTotal' =>$totalRecord,
        'recordsFiltered' =>$totalRecord,
        'data' => $students,
        'regex'=>false]);
    }
};
