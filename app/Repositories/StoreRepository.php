<?php

namespace App\Repositories;

use App\Interfaces\StoreRepositoryInterface;
use App\Models\Store;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreRepository implements StoreRepositoryInterface{

    use RelationTraits;
    public function all()
    {
        return Store::all();
    }
    public function create(Request $request)
    {
        $product = $request->input('products');
        return Store::create($request->all())->products()->attach($product);
        // return Store::create($request->all());

    }
    public function update($id, Request $request)
    {
        // $product = $request->input('products');
        $store = Store::find($id);
        $store->update ($request->all());

    }
    public function delete($id)
    {
        Store::find($id)->delete();
    }
    public function fetchStores(Request $request){
        $start = $request->all();

        $query = DB::table('stores');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }

        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $store=$query->offset($start['start'])
        ->limit($start['length'])
        ->get();

        $totalRecord = Store::count();
        // dd($leads);
        return response()->json(['recordsTotal' =>$totalRecord,
        'recordsFiltered' =>$totalRecord,
        'data' => $store,
    'regex'=>false]);
    }
};
