<?php

namespace App\Repositories;

use App\Interfaces\ProjectRepositoryInterface;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectRepository implements ProjectRepositoryInterface{

    public function all()
    {
        return Project::all();
    }
    public function create(Request $request)
    {
        return Project::create($request->all());
    }
    public function update($id, Request $request)
    {
        $project = Project::find($id)->update($request->all());
        // $product = Product::find($id);
        // $product->update($request->all());


    }
    public function delete($id)
    {
        Project::find($id)->delete();
    }
    public function fetchProjects(Request $request)
    {

        $start = $request->all();

        $query = DB::table('projects');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }

        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }
        
        $project = $query->offset($start['start'])
            ->limit($start['length'])
            ->get();

            $totalRecord = Project::count();

            return response()->json(['recordsTotal' => $totalRecord,
            'recordsFiltered' => $totalRecord,
            'data' => $project,
            'regex'=>false]);
    }
};
