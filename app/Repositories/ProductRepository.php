<?php

namespace App\Repositories;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductRepository implements ProductRepositoryInterface{

    use RelationTraits;
    public function all()
    {
        return Product::all();
    }
    public function create(Request $request)
    {
        $store = $request->input('stores');
        return Product::create($request->all())->stores()->attach($store);
        // $product->stores()->attach($store);
        // return Product::create($request->all());
    }
    public function update($id, Request $request)
    {

        $store = Product::find($id)->update($request->all());

    }
    public function delete($id)
    {
        Product::find($id)->delete();
    }
    public function fetchProducts(Request $request)
    {
        $start = $request->all();
        // dd($start);

        $query = DB::table('products');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }

        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $product = $query->offset($start['start'])
        ->limit($start['length'])
        ->get();

        $totalRecord = Product::count();
        return response()->json(['recordsTotal' =>$totalRecord,
        'recordsFiltered' => $totalRecord,
        'data' => $product,
        'regex'=>false]);
    }
};
