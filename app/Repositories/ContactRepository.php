<?php

namespace App\Repositories;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Traits\RelationTraits;
use App\Interfaces\ContactRepositoryInterface;

class ContactRepository implements ContactRepositoryInterface{

    use RelationTraits;
    public function all()
    {
        return Contact::all();
    }
    public function create(Request $request)
    {
        // dump($request->toArray());
        $account = $request->input('accounts');
        return Contact::create($request->all())->accounts()->attach($account);
        // dd($a);
        // return Contact::create($request->all());

    }
    public function update($id, Request $request)
    {
        $contact = Contact::find($id)->update($request->all());
        // $contact->update($request->all());
        // return Contact::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Contact::find($id)->delete();
    }
    public function fetchContacts(Request $request){
        $start = $request->all();

        // $query = DB::table('contacts');
        // $contacts = Contact::select('contacts.*', 'accounts.name AS accounts_name')->join('accounts', 'accounts.id', 'contacts.account_id')->get();
        $query = Contact::select('contacts.*', 'accounts.name AS accounts_name')->join('accounts', 'accounts.id', 'contacts.account_id');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $columnData = $column['data'];
                    if ($columnData === 'accounts_name') {
                        $query->orWhere('accounts.name', 'like', "%$searchTerm%");
                    } elseif (str_contains($columnData, '.')) {
                        // If column already contains a dot, assume it's fully qualified
                        $query->orWhere($columnData, 'like', "%$searchTerm%");
                    } else {
                        // Otherwise, assume it's from the 'contacts' table
                        $query->orWhere('contacts.'.$columnData, 'like', "%$searchTerm%");
                    }
                }
            });
        }
        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }
            $contacts = $query->offset($start['start'])
            ->limit($start['length'])
            ->get();


        $totalRecord = Contact::count();
        return response()->json([
            'recordsTotal' => $totalRecord,
            'recordsFiltered' => $totalRecord,
            'data' => $contacts,
            'regex'=>false]);
    }
};
