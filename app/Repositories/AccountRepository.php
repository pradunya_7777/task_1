<?php

namespace App\Repositories;

use App\Http\Requests\StoreAccountRequest;
use App\Interfaces\AccountRepositoryInterface;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountRepository implements AccountRepositoryInterface
{

    public function all()
    {
        return Account::all();
    }
    public function create(Request $request)
    {
        return Account::create($request->all());;
    }
    public function update($id, Request $request)
    {
        $account = Account::find($id);
        $account->update($request->all());
        // return Account::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Account::find($id)->delete();
    }

    public function fetchAccounts(Request $request)
    {
        $start = $request->all();
        $query = DB::table('accounts');
// dd($start);
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }
        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $accounts = $query->offset($start['start'])
            ->limit($start['length'])
            ->get();

        $totalRecord = Account::count();

        return response()->json([
            'recordsTotal' => $totalRecord,
            'recordsFiltered' => $totalRecord,
            'data' => $accounts,
            'regex' => false
        ]);
    }
};
