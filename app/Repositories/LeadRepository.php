<?php

namespace App\Repositories;

use App\Interfaces\LeadRepositoryInterface;
use App\Models\Lead;
use App\Traits\RelationTraits;
use Illuminate\Http\Request;

class LeadRepository implements LeadRepositoryInterface{

    use RelationTraits;
    public function all()
    {
        return Lead::all();
    }
    public function create(Request $request)
    {
        // dd($request);
        $account = $request->input('account');
        return Lead::create($request->all())->accounts()->attach($account);
    }
    public function update($id, Request $request)
    {
        $lead = Lead::find($id)->update($request->all());
        // return Contact::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Lead::find($id)->delete();
    }
    public function fetchLeads(Request $request){
        $start = $request->all();

        $query = Lead::select('leads.*', 'accounts.name AS accounts_name')->join('accounts', 'accounts.id', 'leads.account_id');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $columnData = $column['data'];
                    if ($columnData === 'accounts_name') {
                        $query->orWhere('accounts.name', 'like', "%$searchTerm%");
                    } elseif (str_contains($columnData, '.')) {
                        // If column already contains a dot, assume it's fully qualified
                        $query->orWhere($columnData, 'like', "%$searchTerm%");
                    } else {
                        // Otherwise, assume it's from the 'contacts' table
                        $query->orWhere('leads.'.$columnData, 'like', "%$searchTerm%");
                    }
                }
            });
        }
        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $leads = $query->offset($start['start'])
        ->limit($start['length'])
        ->get();
        $totalRecord = Lead::count();

        // dd($totalRecord);
        return response()->json(['recordsTotal' =>$totalRecord,
        'recordsFiltered' => $totalRecord,
        'data' => $leads,
    'regex'=>false]);
    }
};
