<?php

namespace App\Repositories;

use App\Http\Requests\StoreAcitivityRequest;
use App\Http\Requests\StoreContactsRequest;
use App\Interfaces\ActivityRepositoryInterface;
use App\Models\Activity;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityRepository implements ActivityRepositoryInterface
{

    public function all()
    {
        return Activity::all();
    }
    public function create(Request $request)
    {
        // echo $request->file('txt')->storeAs('files',$filename);
        return Activity::create($request->all());
    }
    public function update($id, Request $request)
    {
        $activity = Activity::find($id);
        $activity->update($request->all());
        // return Contact::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Activity::find($id)->delete();
    }
    public function fetchActivities(Request $request)
    {
        $start = $request->all();

        $query = DB::table('activities');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }
        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }

        $activities = $query->offset($start['start'])
            ->limit($start['length'])
            ->get();

        $totalRecord = Activity::count();

        return response()->json([
            'recordsTotal' => $totalRecord,
            'recordsFiltered' => $totalRecord,
            'data' => $activities,
            'regex' => false]);
    }
};
