<?php

namespace App\Repositories;

use App\Interfaces\TutionRepositoryInterface;
use App\Models\Tution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutionRepository implements TutionRepositoryInterface{

    public function all()
    {
        return Tution::all();
    }
    public function create(Request $request)
    {
        $student = $request->input('students');
        return Tution::create($request->all())->students()->attach($student);
        // $product->stores()->attach($store);
        // return Product::create($request->all());
    }
    public function update($id, Request $request)
    {
        $store = $request->input('stores');
        $product = Tution::find($id)->update($request->all());
        $product->stores()->attach($store);
        // $product = Product::find($id);
        // $product->update($request->all());
        // return Contact::whereId($id)->update($request->all());


    }
    public function delete($id)
    {
        Tution::find($id)->delete();
    }
    public function fetchTutions(Request $request){
        $start = $request->all();
        $query = DB::table('tutions');
        if (!empty($start['search']['value']) && !empty($start['columns'])) {
            $searchTerm = $start['search']['value'];

            $query->where(function ($query) use ($searchTerm, $start) {
                foreach ($start['columns'] as $column) {
                    $query->orWhere($column['data'], 'like', "%$searchTerm%");
                }
            });
        }

        if (!empty($start['order'])) {
            $orderColumn = $start['columns'][$start['order'][0]['column']]['data'];
            $orderDirection = $start['order'][0]['dir'];
            $query->orderBy($orderColumn, $orderDirection);
        }
        
        $tutions=$query->offset($start['start'])
        ->limit($start['length'])
        ->get();
        $totalRecord = Tution::all();
        return response()->json(['recordsTotal' =>count($totalRecord), 'recordsFiltered' => count($totalRecord), 'data' => $tutions]);
    }
};
