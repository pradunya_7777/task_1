/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/product.js":
/*!*********************************!*\
  !*** ./resources/js/product.js ***!
  \*********************************/
/***/ (() => {

eval("$(function () {\n  $(\"#table\").DataTable({\n    \"scrollY\": \"370px\",\n    ajax: \"/products/fetch\",\n    type: \"GET\",\n    columns: [{\n      data: \"name\",\n      render: function render(data, type, row) {\n        return '<a href=\"/products/' + row.id + '\">' + row.name + '</a>';\n        return data;\n      }\n    }, {\n      data: 'category'\n    }, {\n      data: 'brand'\n    }, {\n      data: 'price'\n    }],\n    processing: true,\n    serverSide: true\n  });\n  $('.table_filter').on('keyup', function () {\n    table.search(this.value).draw();\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvcHJvZHVjdC5qcyIsIm5hbWVzIjpbIiQiLCJEYXRhVGFibGUiLCJhamF4IiwidHlwZSIsImNvbHVtbnMiLCJkYXRhIiwicmVuZGVyIiwicm93IiwiaWQiLCJuYW1lIiwicHJvY2Vzc2luZyIsInNlcnZlclNpZGUiLCJvbiIsInRhYmxlIiwic2VhcmNoIiwidmFsdWUiLCJkcmF3Il0sInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcHJvZHVjdC5qcz9mYzYyIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZnVuY3Rpb24gKCkge1xuICAgICQoXCIjdGFibGVcIikuRGF0YVRhYmxlKHtcblxuICAgICAgICBcInNjcm9sbFlcIjpcIjM3MHB4XCIsXG5cbiAgICAgICAgYWpheDogXCIvcHJvZHVjdHMvZmV0Y2hcIixcbiAgICAgICAgdHlwZTogXCJHRVRcIixcbiAgICAgICAgY29sdW1uczogW1xuICAgICAgICAgICAgeyBkYXRhOiBcIm5hbWVcIixcbiAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24gKGRhdGEsIHR5cGUsIHJvdykge1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuICc8YSBocmVmPVwiL3Byb2R1Y3RzLycrIHJvdy5pZCArJ1wiPicrIHJvdy5uYW1lICsgJzwvYT4nO1xuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgeyBkYXRhOidjYXRlZ29yeScgfSxcbiAgICAgICAgICAgIHsgZGF0YTonYnJhbmQnIH0sXG4gICAgICAgICAgICB7IGRhdGE6J3ByaWNlJyB9LFxuICAgICAgICBdLFxuICAgICAgICBwcm9jZXNzaW5nOiB0cnVlLFxuICAgICAgICBzZXJ2ZXJTaWRlOiB0cnVlLFxuICAgIH0pO1xuICAgICQoJy50YWJsZV9maWx0ZXInKS5vbiggJ2tleXVwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB0YWJsZS5zZWFyY2goIHRoaXMudmFsdWUgKS5kcmF3KCk7XG4gICAgfSk7XG59KTtcbiJdLCJtYXBwaW5ncyI6IkFBQUFBLENBQUMsQ0FBQyxZQUFZO0VBQ1ZBLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQ0MsU0FBUyxDQUFDO0lBRWxCLFNBQVMsRUFBQyxPQUFPO0lBRWpCQyxJQUFJLEVBQUUsaUJBQWlCO0lBQ3ZCQyxJQUFJLEVBQUUsS0FBSztJQUNYQyxPQUFPLEVBQUUsQ0FDTDtNQUFFQyxJQUFJLEVBQUUsTUFBTTtNQUNkQyxNQUFNLEVBQUUsU0FBQUEsT0FBVUQsSUFBSSxFQUFFRixJQUFJLEVBQUVJLEdBQUcsRUFBRTtRQUUvQixPQUFPLHFCQUFxQixHQUFFQSxHQUFHLENBQUNDLEVBQUUsR0FBRSxJQUFJLEdBQUVELEdBQUcsQ0FBQ0UsSUFBSSxHQUFHLE1BQU07UUFDakUsT0FBT0osSUFBSTtNQUNYO0lBQ0EsQ0FBQyxFQUNEO01BQUVBLElBQUksRUFBQztJQUFXLENBQUMsRUFDbkI7TUFBRUEsSUFBSSxFQUFDO0lBQVEsQ0FBQyxFQUNoQjtNQUFFQSxJQUFJLEVBQUM7SUFBUSxDQUFDLENBQ25CO0lBQ0RLLFVBQVUsRUFBRSxJQUFJO0lBQ2hCQyxVQUFVLEVBQUU7RUFDaEIsQ0FBQyxDQUFDO0VBQ0ZYLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQ1ksRUFBRSxDQUFFLE9BQU8sRUFBRSxZQUFZO0lBQ3hDQyxLQUFLLENBQUNDLE1BQU0sQ0FBRSxJQUFJLENBQUNDLEtBQU0sQ0FBQyxDQUFDQyxJQUFJLENBQUMsQ0FBQztFQUNyQyxDQUFDLENBQUM7QUFDTixDQUFDLENBQUMifQ==\n//# sourceURL=webpack-internal:///./resources/js/product.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/product.js"]();
/******/ 	
/******/ })()
;