/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/accountupdate.js":
/*!***************************************!*\
  !*** ./resources/js/accountupdate.js ***!
  \***************************************/
/***/ (() => {

eval("$(document).ready(function () {\n  // Handle form submission\n  $('#update-form').on('click', function () {\n    // Prevent the default form submission\n\n    // Serialize the form data\n    var formData = $(this).serialize();\n\n    // Make an AJAX request\n    $.ajax({\n      url: $(this).attr('action'),\n      // Get the action URL from the form's action attribute\n      type: 'POST',\n      data: formData,\n      success: function success(response) {}\n    });\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvYWNjb3VudHVwZGF0ZS5qcyIsIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5Iiwib24iLCJmb3JtRGF0YSIsInNlcmlhbGl6ZSIsImFqYXgiLCJ1cmwiLCJhdHRyIiwidHlwZSIsImRhdGEiLCJzdWNjZXNzIiwicmVzcG9uc2UiXSwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9hY2NvdW50dXBkYXRlLmpzP2Y5MWIiXSwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgLy8gSGFuZGxlIGZvcm0gc3VibWlzc2lvblxuICAgICQoJyN1cGRhdGUtZm9ybScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgLy8gUHJldmVudCB0aGUgZGVmYXVsdCBmb3JtIHN1Ym1pc3Npb25cblxuICAgICAgICAvLyBTZXJpYWxpemUgdGhlIGZvcm0gZGF0YVxuICAgICAgICB2YXIgZm9ybURhdGEgPSAkKHRoaXMpLnNlcmlhbGl6ZSgpO1xuXG4gICAgICAgIC8vIE1ha2UgYW4gQUpBWCByZXF1ZXN0XG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB1cmw6ICQodGhpcykuYXR0cignYWN0aW9uJyksIC8vIEdldCB0aGUgYWN0aW9uIFVSTCBmcm9tIHRoZSBmb3JtJ3MgYWN0aW9uIGF0dHJpYnV0ZVxuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsXG4gICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSkge1xuXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG4iXSwibWFwcGluZ3MiOiJBQUFBQSxDQUFDLENBQUNDLFFBQVEsQ0FBQyxDQUFDQyxLQUFLLENBQUMsWUFBVztFQUN6QjtFQUNBRixDQUFDLENBQUMsY0FBYyxDQUFDLENBQUNHLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBVztJQUNwQzs7SUFFRDtJQUNBLElBQUlDLFFBQVEsR0FBR0osQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDSyxTQUFTLENBQUMsQ0FBQzs7SUFFbEM7SUFDQUwsQ0FBQyxDQUFDTSxJQUFJLENBQUM7TUFDSEMsR0FBRyxFQUFFUCxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUNRLElBQUksQ0FBQyxRQUFRLENBQUM7TUFBRTtNQUM3QkMsSUFBSSxFQUFFLE1BQU07TUFDWkMsSUFBSSxFQUFFTixRQUFRO01BQ2RPLE9BQU8sRUFBRSxTQUFBQSxRQUFTQyxRQUFRLEVBQUUsQ0FFNUI7SUFDSixDQUFDLENBQUM7RUFDTixDQUFDLENBQUM7QUFDTixDQUFDLENBQUMifQ==\n//# sourceURL=webpack-internal:///./resources/js/accountupdate.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/accountupdate.js"]();
/******/ 	
/******/ })()
;