/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/contactupdate.js":
/*!***************************************!*\
  !*** ./resources/js/contactupdate.js ***!
  \***************************************/
/***/ (() => {

eval("$(function () {\n  // Handle form submission\n  $('#update-form').on('submit', function () {\n    // Prevent the default form submission\n\n    // Serialize the form data\n    var formData = $(this).serialize();\n\n    // Make an AJAX request\n    $.ajax({\n      url: $(this).attr('action'),\n      // Get the action URL from the form's action attribute\n      type: 'POST',\n      data: formData,\n      success: function success(response) {}\n    });\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvY29udGFjdHVwZGF0ZS5qcyIsIm5hbWVzIjpbIiQiLCJvbiIsImZvcm1EYXRhIiwic2VyaWFsaXplIiwiYWpheCIsInVybCIsImF0dHIiLCJ0eXBlIiwiZGF0YSIsInN1Y2Nlc3MiLCJyZXNwb25zZSJdLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2NvbnRhY3R1cGRhdGUuanM/M2YzZSJdLCJzb3VyY2VzQ29udGVudCI6WyIkKGZ1bmN0aW9uKCkge1xuICAgIC8vIEhhbmRsZSBmb3JtIHN1Ym1pc3Npb25cbiAgICAkKCcjdXBkYXRlLWZvcm0nKS5vbignc3VibWl0JywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAvLyBQcmV2ZW50IHRoZSBkZWZhdWx0IGZvcm0gc3VibWlzc2lvblxuXG4gICAgICAgIC8vIFNlcmlhbGl6ZSB0aGUgZm9ybSBkYXRhXG4gICAgICAgIHZhciBmb3JtRGF0YSA9ICQodGhpcykuc2VyaWFsaXplKCk7XG5cbiAgICAgICAgLy8gTWFrZSBhbiBBSkFYIHJlcXVlc3RcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHVybDogJCh0aGlzKS5hdHRyKCdhY3Rpb24nKSwgLy8gR2V0IHRoZSBhY3Rpb24gVVJMIGZyb20gdGhlIGZvcm0ncyBhY3Rpb24gYXR0cmlidXRlXG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59KTtcbiJdLCJtYXBwaW5ncyI6IkFBQUFBLENBQUMsQ0FBQyxZQUFXO0VBQ1Q7RUFDQUEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDQyxFQUFFLENBQUMsUUFBUSxFQUFFLFlBQVc7SUFDckM7O0lBRUQ7SUFDQSxJQUFJQyxRQUFRLEdBQUdGLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQ0csU0FBUyxDQUFDLENBQUM7O0lBRWxDO0lBQ0FILENBQUMsQ0FBQ0ksSUFBSSxDQUFDO01BQ0hDLEdBQUcsRUFBRUwsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDTSxJQUFJLENBQUMsUUFBUSxDQUFDO01BQUU7TUFDN0JDLElBQUksRUFBRSxNQUFNO01BQ1pDLElBQUksRUFBRU4sUUFBUTtNQUNkTyxPQUFPLEVBQUUsU0FBQUEsUUFBU0MsUUFBUSxFQUFFLENBRTVCO0lBQ0osQ0FBQyxDQUFDO0VBQ04sQ0FBQyxDQUFDO0FBQ04sQ0FBQyxDQUFDIn0=\n//# sourceURL=webpack-internal:///./resources/js/contactupdate.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/contactupdate.js"]();
/******/ 	
/******/ })()
;