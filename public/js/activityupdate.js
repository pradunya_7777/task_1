/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/activityupdate.js":
/*!****************************************!*\
  !*** ./resources/js/activityupdate.js ***!
  \****************************************/
/***/ (() => {

eval("$(function () {\n  // Handle form submission\n  $('#update-form').on('submit', function () {\n    // Prevent the default form submission\n\n    // Serialize the form data\n    var formData = $(this).serialize();\n\n    // Make an AJAX request\n    $.ajax({\n      url: $(this).attr('action'),\n      // Get the action URL from the form's action attribute\n      type: 'POST',\n      data: formData,\n      success: function success(response) {}\n    });\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvYWN0aXZpdHl1cGRhdGUuanMiLCJuYW1lcyI6WyIkIiwib24iLCJmb3JtRGF0YSIsInNlcmlhbGl6ZSIsImFqYXgiLCJ1cmwiLCJhdHRyIiwidHlwZSIsImRhdGEiLCJzdWNjZXNzIiwicmVzcG9uc2UiXSwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9hY3Rpdml0eXVwZGF0ZS5qcz84MzNmIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZnVuY3Rpb24oKSB7XG4gICAgLy8gSGFuZGxlIGZvcm0gc3VibWlzc2lvblxuICAgICQoJyN1cGRhdGUtZm9ybScpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgIC8vIFByZXZlbnQgdGhlIGRlZmF1bHQgZm9ybSBzdWJtaXNzaW9uXG5cbiAgICAgICAgLy8gU2VyaWFsaXplIHRoZSBmb3JtIGRhdGFcbiAgICAgICAgdmFyIGZvcm1EYXRhID0gJCh0aGlzKS5zZXJpYWxpemUoKTtcblxuICAgICAgICAvLyBNYWtlIGFuIEFKQVggcmVxdWVzdFxuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdXJsOiAkKHRoaXMpLmF0dHIoJ2FjdGlvbicpLCAvLyBHZXQgdGhlIGFjdGlvbiBVUkwgZnJvbSB0aGUgZm9ybSdzIGFjdGlvbiBhdHRyaWJ1dGVcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQUEsQ0FBQyxDQUFDLFlBQVc7RUFDVDtFQUNBQSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUNDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsWUFBVztJQUNyQzs7SUFFRDtJQUNBLElBQUlDLFFBQVEsR0FBR0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDRyxTQUFTLENBQUMsQ0FBQzs7SUFFbEM7SUFDQUgsQ0FBQyxDQUFDSSxJQUFJLENBQUM7TUFDSEMsR0FBRyxFQUFFTCxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUNNLElBQUksQ0FBQyxRQUFRLENBQUM7TUFBRTtNQUM3QkMsSUFBSSxFQUFFLE1BQU07TUFDWkMsSUFBSSxFQUFFTixRQUFRO01BQ2RPLE9BQU8sRUFBRSxTQUFBQSxRQUFTQyxRQUFRLEVBQUUsQ0FFNUI7SUFDSixDQUFDLENBQUM7RUFDTixDQUFDLENBQUM7QUFDTixDQUFDLENBQUMifQ==\n//# sourceURL=webpack-internal:///./resources/js/activityupdate.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/activityupdate.js"]();
/******/ 	
/******/ })()
;