/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/activity.js":
/*!**********************************!*\
  !*** ./resources/js/activity.js ***!
  \**********************************/
/***/ (() => {

eval("$(function () {\n  $(\"#table\").DataTable({\n    \"scrollX\": true,\n    \"scrollY\": \"370px\",\n    \"scrollCollapse\": true,\n    ajax: \"/activities/fetch\",\n    type: \"GET\",\n    columns: [{\n      data: \"subject\",\n      render: function render(data, type, row) {\n        return '<a href=\"/activities/' + row.id + '\">' + row.subject + '</a>';\n        return data;\n      }\n    }, {\n      data: \"relatedTo\"\n    }, {\n      data: \"start\"\n    }, {\n      data: \"end\"\n    }, {\n      data: \"taskType\"\n    }, {\n      data: \"status\"\n    }, {\n      data: \"description\"\n    }],\n    processing: true,\n    serverSide: true\n  });\n  $('.table_filter').on('keyup', function () {\n    table.search(this.value).draw();\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvYWN0aXZpdHkuanMiLCJuYW1lcyI6WyIkIiwiRGF0YVRhYmxlIiwiYWpheCIsInR5cGUiLCJjb2x1bW5zIiwiZGF0YSIsInJlbmRlciIsInJvdyIsImlkIiwic3ViamVjdCIsInByb2Nlc3NpbmciLCJzZXJ2ZXJTaWRlIiwib24iLCJ0YWJsZSIsInNlYXJjaCIsInZhbHVlIiwiZHJhdyJdLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2FjdGl2aXR5LmpzP2IyNDQiXSwic291cmNlc0NvbnRlbnQiOlsiJChmdW5jdGlvbiAoKSB7XG4gICAgJChcIiN0YWJsZVwiKS5EYXRhVGFibGUoe1xuICAgICAgICBcInNjcm9sbFhcIjogdHJ1ZSxcbiAgICAgICAgXCJzY3JvbGxZXCI6IFwiMzcwcHhcIixcbiAgICAgICAgXCJzY3JvbGxDb2xsYXBzZVwiOiB0cnVlLFxuXG4gICAgICAgIGFqYXg6IFwiL2FjdGl2aXRpZXMvZmV0Y2hcIixcbiAgICAgICAgdHlwZTogXCJHRVRcIixcbiAgICAgICAgY29sdW1uczogW3sgZGF0YTogXCJzdWJqZWN0XCIgLFxuICAgICAgICAgICAgcmVuZGVyOiBmdW5jdGlvbiAoZGF0YSwgdHlwZSwgcm93KSB7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCIvYWN0aXZpdGllcy8nKyByb3cuaWQgKydcIj4nKyByb3cuc3ViamVjdCArICc8L2E+JztcblxuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJyZWxhdGVkVG9cIiB9LFxuICAgICAgICAgICAgeyBkYXRhOiBcInN0YXJ0XCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJlbmRcIiB9LFxuICAgICAgICAgICAgeyBkYXRhOiBcInRhc2tUeXBlXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJzdGF0dXNcIiB9LFxuICAgICAgICAgICAgeyBkYXRhOiBcImRlc2NyaXB0aW9uXCIgfSxcbiAgICAgICAgXSxcbiAgICAgICAgcHJvY2Vzc2luZzogdHJ1ZSxcbiAgICAgICAgc2VydmVyU2lkZTogdHJ1ZSxcbiAgICB9KTtcbiAgICAkKCcudGFibGVfZmlsdGVyJykub24oICdrZXl1cCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGFibGUuc2VhcmNoKCB0aGlzLnZhbHVlICkuZHJhdygpO1xuICAgIH0pO1xufSk7XG4iXSwibWFwcGluZ3MiOiJBQUFBQSxDQUFDLENBQUMsWUFBWTtFQUNWQSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUNDLFNBQVMsQ0FBQztJQUNsQixTQUFTLEVBQUUsSUFBSTtJQUNmLFNBQVMsRUFBRSxPQUFPO0lBQ2xCLGdCQUFnQixFQUFFLElBQUk7SUFFdEJDLElBQUksRUFBRSxtQkFBbUI7SUFDekJDLElBQUksRUFBRSxLQUFLO0lBQ1hDLE9BQU8sRUFBRSxDQUFDO01BQUVDLElBQUksRUFBRSxTQUFTO01BQ3ZCQyxNQUFNLEVBQUUsU0FBQUEsT0FBVUQsSUFBSSxFQUFFRixJQUFJLEVBQUVJLEdBQUcsRUFBRTtRQUUvQixPQUFPLHVCQUF1QixHQUFFQSxHQUFHLENBQUNDLEVBQUUsR0FBRSxJQUFJLEdBQUVELEdBQUcsQ0FBQ0UsT0FBTyxHQUFHLE1BQU07UUFFdEUsT0FBT0osSUFBSTtNQUNYO0lBQ0MsQ0FBQyxFQUNGO01BQUVBLElBQUksRUFBRTtJQUFZLENBQUMsRUFDckI7TUFBRUEsSUFBSSxFQUFFO0lBQVEsQ0FBQyxFQUNqQjtNQUFFQSxJQUFJLEVBQUU7SUFBTSxDQUFDLEVBQ2Y7TUFBRUEsSUFBSSxFQUFFO0lBQVcsQ0FBQyxFQUNwQjtNQUFFQSxJQUFJLEVBQUU7SUFBUyxDQUFDLEVBQ2xCO01BQUVBLElBQUksRUFBRTtJQUFjLENBQUMsQ0FDMUI7SUFDREssVUFBVSxFQUFFLElBQUk7SUFDaEJDLFVBQVUsRUFBRTtFQUNoQixDQUFDLENBQUM7RUFDRlgsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDWSxFQUFFLENBQUUsT0FBTyxFQUFFLFlBQVk7SUFDeENDLEtBQUssQ0FBQ0MsTUFBTSxDQUFFLElBQUksQ0FBQ0MsS0FBTSxDQUFDLENBQUNDLElBQUksQ0FBQyxDQUFDO0VBQ3JDLENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQyJ9\n//# sourceURL=webpack-internal:///./resources/js/activity.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/activity.js"]();
/******/ 	
/******/ })()
;