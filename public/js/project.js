/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/project.js":
/*!*********************************!*\
  !*** ./resources/js/project.js ***!
  \*********************************/
/***/ (() => {

eval("$(function () {\n  $(\"#table\").DataTable({\n    \"scrollY\": \"370px\",\n    \"scrollCollapse\": true,\n    ajax: \"/projects/fetch\",\n    type: \"GET\",\n    columns: [{\n      data: \"name\",\n      render: function render(data, type, row) {\n        return '<a href=\"/projects/' + row.id + '\">' + row.name + \"</a>\";\n        return data;\n      }\n    }, {\n      data: \"priority\"\n    }, {\n      data: \"project_type\"\n    }, {\n      data: \"start_date\"\n    }, {\n      data: \"due_date\"\n    }],\n    processing: true,\n    serverSide: true\n  });\n  $('.table_filter').on('keyup', function () {\n    table.search(this.value).draw();\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvcHJvamVjdC5qcyIsIm5hbWVzIjpbIiQiLCJEYXRhVGFibGUiLCJhamF4IiwidHlwZSIsImNvbHVtbnMiLCJkYXRhIiwicmVuZGVyIiwicm93IiwiaWQiLCJuYW1lIiwicHJvY2Vzc2luZyIsInNlcnZlclNpZGUiLCJvbiIsInRhYmxlIiwic2VhcmNoIiwidmFsdWUiLCJkcmF3Il0sInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcHJvamVjdC5qcz8yMjk0Il0sInNvdXJjZXNDb250ZW50IjpbIiQoZnVuY3Rpb24gKCkge1xuICAgICQoXCIjdGFibGVcIikuRGF0YVRhYmxlKHtcblxuICAgICAgICBcInNjcm9sbFlcIjpcIjM3MHB4XCIsXG4gICAgICAgIFwic2Nyb2xsQ29sbGFwc2VcIjogdHJ1ZSxcblxuXG4gICAgICAgIGFqYXg6IFwiL3Byb2plY3RzL2ZldGNoXCIsXG4gICAgICAgIHR5cGU6IFwiR0VUXCIsXG4gICAgICAgIGNvbHVtbnM6IFtcbiAgICAgICAgICAgIHsgZGF0YTogXCJuYW1lXCIsXG4gICAgICAgICAgICByZW5kZXI6IGZ1bmN0aW9uIChkYXRhLCB0eXBlLCByb3cpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAnPGEgaHJlZj1cIi9wcm9qZWN0cy8nICtcbiAgICAgICAgICAgICAgICAgICAgcm93LmlkICtcbiAgICAgICAgICAgICAgICAgICAgJ1wiPicgK1xuICAgICAgICAgICAgICAgICAgICByb3cubmFtZSArXG4gICAgICAgICAgICAgICAgICAgIFwiPC9hPlwiXG4gICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJwcmlvcml0eVwiIH0sXG4gICAgICAgICAgICB7IGRhdGE6IFwicHJvamVjdF90eXBlXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJzdGFydF9kYXRlXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJkdWVfZGF0ZVwiIH1cbiAgICAgICAgXSxcbiAgICAgICAgcHJvY2Vzc2luZzogdHJ1ZSxcbiAgICAgICAgc2VydmVyU2lkZTogdHJ1ZSxcbiAgICB9KTtcbiAgICAkKCcudGFibGVfZmlsdGVyJykub24oICdrZXl1cCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGFibGUuc2VhcmNoKCB0aGlzLnZhbHVlICkuZHJhdygpO1xuICAgIH0pO1xufSk7XG4iXSwibWFwcGluZ3MiOiJBQUFBQSxDQUFDLENBQUMsWUFBWTtFQUNWQSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUNDLFNBQVMsQ0FBQztJQUVsQixTQUFTLEVBQUMsT0FBTztJQUNqQixnQkFBZ0IsRUFBRSxJQUFJO0lBR3RCQyxJQUFJLEVBQUUsaUJBQWlCO0lBQ3ZCQyxJQUFJLEVBQUUsS0FBSztJQUNYQyxPQUFPLEVBQUUsQ0FDTDtNQUFFQyxJQUFJLEVBQUUsTUFBTTtNQUNkQyxNQUFNLEVBQUUsU0FBQUEsT0FBVUQsSUFBSSxFQUFFRixJQUFJLEVBQUVJLEdBQUcsRUFBRTtRQUMvQixPQUNJLHFCQUFxQixHQUNyQkEsR0FBRyxDQUFDQyxFQUFFLEdBQ04sSUFBSSxHQUNKRCxHQUFHLENBQUNFLElBQUksR0FDUixNQUFNO1FBR1YsT0FBT0osSUFBSTtNQUNmO0lBQ0osQ0FBQyxFQUNHO01BQUVBLElBQUksRUFBRTtJQUFXLENBQUMsRUFDcEI7TUFBRUEsSUFBSSxFQUFFO0lBQWUsQ0FBQyxFQUN4QjtNQUFFQSxJQUFJLEVBQUU7SUFBYSxDQUFDLEVBQ3RCO01BQUVBLElBQUksRUFBRTtJQUFXLENBQUMsQ0FDdkI7SUFDREssVUFBVSxFQUFFLElBQUk7SUFDaEJDLFVBQVUsRUFBRTtFQUNoQixDQUFDLENBQUM7RUFDRlgsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDWSxFQUFFLENBQUUsT0FBTyxFQUFFLFlBQVk7SUFDeENDLEtBQUssQ0FBQ0MsTUFBTSxDQUFFLElBQUksQ0FBQ0MsS0FBTSxDQUFDLENBQUNDLElBQUksQ0FBQyxDQUFDO0VBQ3JDLENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQyJ9\n//# sourceURL=webpack-internal:///./resources/js/project.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/project.js"]();
/******/ 	
/******/ })()
;