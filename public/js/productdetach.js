/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/productdetach.js":
/*!***************************************!*\
  !*** ./resources/js/productdetach.js ***!
  \***************************************/
/***/ (() => {

eval("$(function () {\n  function makeAJAXRequest(childId, ParentId, Model, Function_Name) {\n    $.ajax({\n      url: \"/products/detachproduct\",\n      type: 'GET',\n      datatype: 'json',\n      data: {\n        child_Id: childId,\n        parent_Id: ParentId,\n        model: Model,\n        function_name: Function_Name\n      },\n      success: function success(response) {\n        console.log(response);\n      }\n    });\n  }\n  $(\".myButton\").on(\"click\", function () {\n    var ParentId = $(\"#hidden\").val();\n    var Model = $(\"#model\").val();\n    var Function_Name = $(\"#function_name\").val();\n\n    // console.log(\"Product-Id\",ParentId);\n    var childId = $(this).data(\"child-id\");\n    // console.log(\"Store-Id:\",childId);\n    makeAJAXRequest(childId, ParentId, Model, Function_Name);\n    location.reload(true);\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvcHJvZHVjdGRldGFjaC5qcyIsIm5hbWVzIjpbIiQiLCJtYWtlQUpBWFJlcXVlc3QiLCJjaGlsZElkIiwiUGFyZW50SWQiLCJNb2RlbCIsIkZ1bmN0aW9uX05hbWUiLCJhamF4IiwidXJsIiwidHlwZSIsImRhdGF0eXBlIiwiZGF0YSIsImNoaWxkX0lkIiwicGFyZW50X0lkIiwibW9kZWwiLCJmdW5jdGlvbl9uYW1lIiwic3VjY2VzcyIsInJlc3BvbnNlIiwiY29uc29sZSIsImxvZyIsIm9uIiwidmFsIiwibG9jYXRpb24iLCJyZWxvYWQiXSwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9wcm9kdWN0ZGV0YWNoLmpzPzVkNmYiXSwic291cmNlc0NvbnRlbnQiOlsiJChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gbWFrZUFKQVhSZXF1ZXN0KGNoaWxkSWQsUGFyZW50SWQsTW9kZWwsRnVuY3Rpb25fTmFtZSl7XG5cbiAgICAgICQuYWpheCh7XG4gICAgICAgIHVybDogXCIvcHJvZHVjdHMvZGV0YWNocHJvZHVjdFwiLFxuICAgICAgICB0eXBlOiAnR0VUJyxcbiAgICAgICAgZGF0YXR5cGU6ICdqc29uJyxcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgY2hpbGRfSWQ6IGNoaWxkSWQsXG4gICAgICAgICAgICBwYXJlbnRfSWQ6IFBhcmVudElkLFxuICAgICAgICAgICAgbW9kZWw6TW9kZWwsXG4gICAgICAgICAgICBmdW5jdGlvbl9uYW1lOiBGdW5jdGlvbl9OYW1lLFxuICAgICAgICB9LFxuICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgICAgfSxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgICQoXCIubXlCdXR0b25cIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBQYXJlbnRJZCA9ICQoXCIjaGlkZGVuXCIpLnZhbCgpO1xuXG4gICAgICAgIHZhciBNb2RlbCA9ICQoXCIjbW9kZWxcIikudmFsKCk7XG5cbiAgICAgICAgdmFyIEZ1bmN0aW9uX05hbWUgPSAkKFwiI2Z1bmN0aW9uX25hbWVcIikudmFsKCk7XG5cbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJQcm9kdWN0LUlkXCIsUGFyZW50SWQpO1xuICAgICAgICAgICAgdmFyIGNoaWxkSWQgPSAkKHRoaXMpLmRhdGEoXCJjaGlsZC1pZFwiKTtcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiU3RvcmUtSWQ6XCIsY2hpbGRJZCk7XG4gICAgICAgIG1ha2VBSkFYUmVxdWVzdChjaGlsZElkLFBhcmVudElkLE1vZGVsLEZ1bmN0aW9uX05hbWUpO1xuICAgICAgICBsb2NhdGlvbi5yZWxvYWQodHJ1ZSk7XG4gICAgfSk7XG5cblxufSk7XG5cbiJdLCJtYXBwaW5ncyI6IkFBQUFBLENBQUMsQ0FBQyxZQUFZO0VBQ1YsU0FBU0MsZUFBZUEsQ0FBQ0MsT0FBTyxFQUFDQyxRQUFRLEVBQUNDLEtBQUssRUFBQ0MsYUFBYSxFQUFDO0lBRTVETCxDQUFDLENBQUNNLElBQUksQ0FBQztNQUNMQyxHQUFHLEVBQUUseUJBQXlCO01BQzlCQyxJQUFJLEVBQUUsS0FBSztNQUNYQyxRQUFRLEVBQUUsTUFBTTtNQUNoQkMsSUFBSSxFQUFFO1FBQ0ZDLFFBQVEsRUFBRVQsT0FBTztRQUNqQlUsU0FBUyxFQUFFVCxRQUFRO1FBQ25CVSxLQUFLLEVBQUNULEtBQUs7UUFDWFUsYUFBYSxFQUFFVDtNQUNuQixDQUFDO01BQ0RVLE9BQU8sRUFBRSxTQUFBQSxRQUFVQyxRQUFRLEVBQUU7UUFDekJDLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDRixRQUFRLENBQUM7TUFDekI7SUFDRixDQUFDLENBQUM7RUFDSjtFQUVBaEIsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDbUIsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZO0lBQ25DLElBQUloQixRQUFRLEdBQUdILENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQ29CLEdBQUcsQ0FBQyxDQUFDO0lBRWpDLElBQUloQixLQUFLLEdBQUdKLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQ29CLEdBQUcsQ0FBQyxDQUFDO0lBRTdCLElBQUlmLGFBQWEsR0FBR0wsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUNvQixHQUFHLENBQUMsQ0FBQzs7SUFFN0M7SUFDSSxJQUFJbEIsT0FBTyxHQUFHRixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUNVLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDdEM7SUFDSlQsZUFBZSxDQUFDQyxPQUFPLEVBQUNDLFFBQVEsRUFBQ0MsS0FBSyxFQUFDQyxhQUFhLENBQUM7SUFDckRnQixRQUFRLENBQUNDLE1BQU0sQ0FBQyxJQUFJLENBQUM7RUFDekIsQ0FBQyxDQUFDO0FBR04sQ0FBQyxDQUFDIn0=\n//# sourceURL=webpack-internal:///./resources/js/productdetach.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/productdetach.js"]();
/******/ 	
/******/ })()
;