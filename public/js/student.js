/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/student.js":
/*!*********************************!*\
  !*** ./resources/js/student.js ***!
  \*********************************/
/***/ (() => {

eval("$(function () {\n  $(\"#table\").DataTable({\n    // \"scrollX\": true,\n    \"scrollY\": \"370px\",\n    \"scrollCollapse\": true,\n    ajax: \"/students/fetch\",\n    type: \"GET\",\n    columns: [{\n      data: \"name\",\n      render: function render(data, type, row) {\n        return '<a href=\"/students/' + row.id + '\">' + row.name + \"</a>\";\n        return data;\n      }\n    }, {\n      data: \"gender\"\n    }, {\n      data: \"dob\"\n    }, {\n      data: \"contact\"\n    }, {\n      data: \"class\"\n    }, {\n      data: \"year\"\n    }, {\n      data: \"department\"\n    }],\n    processing: true,\n    serverSide: true\n  });\n  $('.table_filter').on('keyup', function () {\n    table.search(this.value).draw();\n  });\n});\n//'student_name','gender','dob','contact','guardian_name','guardian_contact','class','year','department','address'//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvc3R1ZGVudC5qcyIsIm5hbWVzIjpbIiQiLCJEYXRhVGFibGUiLCJhamF4IiwidHlwZSIsImNvbHVtbnMiLCJkYXRhIiwicmVuZGVyIiwicm93IiwiaWQiLCJuYW1lIiwicHJvY2Vzc2luZyIsInNlcnZlclNpZGUiLCJvbiIsInRhYmxlIiwic2VhcmNoIiwidmFsdWUiLCJkcmF3Il0sInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvc3R1ZGVudC5qcz8xZmQxIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZnVuY3Rpb24gKCkge1xuICAgICQoXCIjdGFibGVcIikuRGF0YVRhYmxlKHtcblxuICAgICAgICAvLyBcInNjcm9sbFhcIjogdHJ1ZSxcbiAgICAgICAgXCJzY3JvbGxZXCI6XCIzNzBweFwiLFxuICAgICAgICBcInNjcm9sbENvbGxhcHNlXCI6IHRydWUsXG5cbiAgICAgICAgYWpheDogXCIvc3R1ZGVudHMvZmV0Y2hcIixcbiAgICAgICAgdHlwZTogXCJHRVRcIixcbiAgICAgICAgY29sdW1uczogW1xuICAgICAgICAgICAgeyBkYXRhOiBcIm5hbWVcIixcbiAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24gKGRhdGEsIHR5cGUsIHJvdykge1xuICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICc8YSBocmVmPVwiL3N0dWRlbnRzLycgK1xuICAgICAgICAgICAgICAgICAgICByb3cuaWQgK1xuICAgICAgICAgICAgICAgICAgICAnXCI+JyArXG4gICAgICAgICAgICAgICAgICAgIHJvdy5uYW1lICtcbiAgICAgICAgICAgICAgICAgICAgXCI8L2E+XCJcbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgIH0sXG4gICAgICAgICAgICB7IGRhdGE6IFwiZ2VuZGVyXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJkb2JcIiB9LFxuICAgICAgICAgICAgeyBkYXRhOiBcImNvbnRhY3RcIiB9LFxuICAgICAgICAgICAgeyBkYXRhOiBcImNsYXNzXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJ5ZWFyXCIgfSxcbiAgICAgICAgICAgIHsgZGF0YTogXCJkZXBhcnRtZW50XCIgfSxcblxuICAgICAgICBdLFxuICAgICAgICBwcm9jZXNzaW5nOiB0cnVlLFxuICAgICAgICBzZXJ2ZXJTaWRlOiB0cnVlLFxuICAgIH0pO1xuICAgICQoJy50YWJsZV9maWx0ZXInKS5vbiggJ2tleXVwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB0YWJsZS5zZWFyY2goIHRoaXMudmFsdWUgKS5kcmF3KCk7XG4gICAgfSk7XG59KTtcbi8vJ3N0dWRlbnRfbmFtZScsJ2dlbmRlcicsJ2RvYicsJ2NvbnRhY3QnLCdndWFyZGlhbl9uYW1lJywnZ3VhcmRpYW5fY29udGFjdCcsJ2NsYXNzJywneWVhcicsJ2RlcGFydG1lbnQnLCdhZGRyZXNzJ1xuIl0sIm1hcHBpbmdzIjoiQUFBQUEsQ0FBQyxDQUFDLFlBQVk7RUFDVkEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDQyxTQUFTLENBQUM7SUFFbEI7SUFDQSxTQUFTLEVBQUMsT0FBTztJQUNqQixnQkFBZ0IsRUFBRSxJQUFJO0lBRXRCQyxJQUFJLEVBQUUsaUJBQWlCO0lBQ3ZCQyxJQUFJLEVBQUUsS0FBSztJQUNYQyxPQUFPLEVBQUUsQ0FDTDtNQUFFQyxJQUFJLEVBQUUsTUFBTTtNQUNkQyxNQUFNLEVBQUUsU0FBQUEsT0FBVUQsSUFBSSxFQUFFRixJQUFJLEVBQUVJLEdBQUcsRUFBRTtRQUMvQixPQUNJLHFCQUFxQixHQUNyQkEsR0FBRyxDQUFDQyxFQUFFLEdBQ04sSUFBSSxHQUNKRCxHQUFHLENBQUNFLElBQUksR0FDUixNQUFNO1FBR1YsT0FBT0osSUFBSTtNQUNmO0lBRUosQ0FBQyxFQUNHO01BQUVBLElBQUksRUFBRTtJQUFTLENBQUMsRUFDbEI7TUFBRUEsSUFBSSxFQUFFO0lBQU0sQ0FBQyxFQUNmO01BQUVBLElBQUksRUFBRTtJQUFVLENBQUMsRUFDbkI7TUFBRUEsSUFBSSxFQUFFO0lBQVEsQ0FBQyxFQUNqQjtNQUFFQSxJQUFJLEVBQUU7SUFBTyxDQUFDLEVBQ2hCO01BQUVBLElBQUksRUFBRTtJQUFhLENBQUMsQ0FFekI7SUFDREssVUFBVSxFQUFFLElBQUk7SUFDaEJDLFVBQVUsRUFBRTtFQUNoQixDQUFDLENBQUM7RUFDRlgsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDWSxFQUFFLENBQUUsT0FBTyxFQUFFLFlBQVk7SUFDeENDLEtBQUssQ0FBQ0MsTUFBTSxDQUFFLElBQUksQ0FBQ0MsS0FBTSxDQUFDLENBQUNDLElBQUksQ0FBQyxDQUFDO0VBQ3JDLENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUNGIn0=\n//# sourceURL=webpack-internal:///./resources/js/student.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/student.js"]();
/******/ 	
/******/ })()
;