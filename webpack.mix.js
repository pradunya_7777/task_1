const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
    mix.js('resources/js/contact.js', 'public/js/contact.js')
    .sourceMaps();
    mix.js('resources/js/contactupdate.js', 'public/js/contactupdate.js')
    .sourceMaps();
    mix.js('resources/js/account.js', 'public/js/account.js')
    .sourceMaps();
    mix.js('resources/js/accountupdate.js', 'public/js/accountupdate.js')
    .sourceMaps();
    mix.js('resources/js/productdetach.js', 'public/js/productdetach.js')
    .sourceMaps();
    mix.js('resources/js/activity.js', 'public/js/activity.js')
    .sourceMaps();
    mix.js('resources/js/activityupdate.js', 'public/js/activityupdate.js')
    .sourceMaps();
    mix.js('resources/js/lead.js', 'public/js/lead.js')
    .sourceMaps();
    mix.js('resources/js/leadupdate.js', 'public/js/leadupdate.js')
    .sourceMaps();
    mix.js('resources/js/product.js', 'public/js/product.js')
    .sourceMaps();
    mix.js('resources/js/store.js', 'public/js/store.js')
    .sourceMaps();
    mix.js('resources/js/project.js', 'public/js/project.js')
    .sourceMaps();
    mix.js('resources/js/student.js', 'public/js/student.js')
    .sourceMaps();
    mix.js('resources/js/tution.js', 'public/js/tution.js')
    .sourceMaps();
    mix.css('resources/DataTables/datatable-custom.css', 'public/DataTables/datatable-custom.css')
    .sourceMaps();
