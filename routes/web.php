<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\CollectiveController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductStoreController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TutionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/account/create', [AccountController::class,'create'])->name('accounts.create');
Route::post('/accounts/store', [AccountController::class,'store'])->name('accounts.store');
Route::get('/account/view', [AccountController::class,'view'])->name('accounts.view');
Route::get('/account/edit/{id}', [AccountController::class,'edit'])->name('accounts.edit');
Route::post('/account/update/{id}', [AccountController::class,'update'])->name('accounts.update');
Route::get('/account/delete/{id}', [AccountController::class,'delete'])->name('accounts.delete');
Route::get('/account/show/{id}', [AccountController::class,'show'])->name('accounts.show');
Route::get('/account/fetch', [AccountController::class,'fetchAccounts']);


Route::get('/contacts/create', [ContactController::class,'create'])->name('contacts.create');
Route::post('/contacts/store', [ContactController::class,'store'])->name('contacts.store');
Route::get('/contacts/view', [ContactController::class,'view'])->name('contacts.view');
Route::get('/contacts/edit/{id}', [ContactController::class,'edit'])->name('contacts.edit');
Route::post('/contacts/update/{id}', [ContactController::class,'update'])->name('contacts.update');
Route::get('/contacts/delete/{id}', [ContactController::class,'delete'])->name('contacts.delete');
Route::get('/contacts/show/{id}', [ContactController::class,'show'])->name('contacts.show');
Route::get('/contacts/fetch', [ContactController::class,'fetchContacts']);

Route::get('/activities/fetch', [ActivityController::class,'fetchActivities']);
Route::resource('/activities',ActivityController::class);

Route::get('/leads/fetch', [LeadController::class,'fetchLeads']);
Route::resource('/leads',LeadController::class);

Route::get('/stores/fetch', [StoreController::class,'fetchStores']);
Route::resource('/stores',StoreController::class);

Route::get('/products/fetch', [ProductController::class,'fetchProducts']);
Route::get('/products/detachproduct', [ProductController::class,'detachproduct']);
Route::resource('/products',ProductController::class);

Route::get('/projects/fetch', [ProjectController::class,'fetchProjects']);
Route::resource('/projects',ProjectController::class);

Route::get('/students/fetch', [StudentController::class,'fetchStudents']);
Route::resource('/students',StudentController::class);

Route::get('/tutions/fetch', [TutionController::class,'fetchTution']);
Route::resource('/tutions',TutionController::class);

Route::view('/test', 'Test1');
