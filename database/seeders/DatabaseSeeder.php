<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\Account::factory()->count(100)->create();
        \App\Models\Activity::factory()->count(100)->create();
        \App\Models\Contact::factory()->count(100)->create();
        \App\Models\Lead::factory()->count(100)->create();
        \App\Models\Product::factory()->count(100)->create();
        \App\Models\Store::factory()->count(100)->create();
        \App\Models\Project::factory()->count(100)->create();
        \App\Models\Tution::factory()->count(100)->create();
        \App\Models\Student::factory()->count(100)->create();
    }
}
