<?php

namespace Database\Seeders;

use App\Models\Tution;
use Illuminate\Database\Seeder;

class TutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Tution::factory()->count(5)->create();
    }
}
