<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;


class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::factory()->count(5)->create();

        //
        // DB::table('accounts')->insert([
        //     'name'=> Str::random(10),
        //     'email'=>Str::random(10).'@gmail.com',
        //     'industry'=>Str::random(10),
        //     'comment_history'=>Str::random(100),
        //     'description_summary'=>Str::random(50),
        // ]);
    }
}
