<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\Contact;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model
     *
     * @var string
     */
    protected $model = Account::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => Str::uuid(),
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber(),
            'partnercode' => $this->faker->numberBetween(100, 999),
            'website' => $this->faker->url(),
            'industry' => $this->faker->word(10),
            'comment_history' => $this->faker->paragraph(1),
            'description_summary' => $this->faker->sentence(5),
        ];

    }

}

