<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'id'=>Str::uuid(),
            'name'=>$this->faker->name(),
            'implementation_status'=>$this->faker->randomElement(['New','Assign','Inprocess','Code Review','Merge Pending','Stage Release','Completed','On hold','Customer Side Pending']),
            'priority'=>$this->faker->randomElement(['High','Medium','Low']),
            'project_type'=>$this->faker->randomElement(['Addon','Fusion','Mobile Application','R & D','POC']),
            'start_date'=>$this->faker->date(),
            'due_date'=>$this->faker->date(),
            'product'=>$this->faker->randomElement(['CRM','CRM Book Subscription','Cloud Telephony','Enjay out mails','Enjay Ensight']),
            'project_hosting_type'=>$this->faker->randomElement(['Enjay cloud','On Site','Internal Project','Client cloud']),
        ];
    }
}
