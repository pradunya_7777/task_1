<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\Contact;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $account = Account::factory()->create();
        return [
            'id' => Str::uuid(),
            'name' => $this->faker->name(),
            'designation' => $this->faker->word(7),
            'email' => $this->faker->safeEmail(),
            'phone' => $this->faker->phoneNumber(),
            'customer_type' => $this->faker->randomElement(['IT Dealer', 'Customer', 'Competitor', 'Currior Comany', 'Not Know']),
            'customer_position' => $this->faker->randomElement(['WSales head', 'Junior', 'Senior', 'Manager', 'Owner']),
            'description' => $this->faker->paragraph(1),
            'comment' => $this->faker->sentence(5),
            'contact_status' => $this->faker->randomElement(['Contacted', 'Not Yet', 'Rejected']),
            'joincommunity' => $this->faker->randomElement(['yes', '']),
            'followupdate' => $this->faker->dateTime(),
            'account_id'=>$account->id,
        ];
    }
    

}
