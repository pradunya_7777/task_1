<?php

namespace Database\Factories;

use App\Models\Account;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeadFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $account = Account::factory()->create();
        return [
            //
            'id'=>Str::uuid(),
            'name'=>$this->faker->name(),
            'account_id'=>$this->faker->numberBetween(1, 10),
            'type'=>$this->faker->randomElement(['IT Dealer','Customer','Competitor','Currior Company','Not Know']),
            'status'=>$this->faker->randomElement(['New', 'In Process','Dead']),
            'phone'=>$this->faker->phoneNumber(),
            'email'=>$this->faker->safeEmail(),
            'product_category'=>$this->faker->randomElement(['Crm','Taly']),
            'no_of_user'=>$this->faker->randomDigit(),
            'industry'=>$this->faker->randomElement(['E-Commerce','Education','Finance & Service','Not Known','Engineering','Other']),
            'lead_source'=>$this->faker->randomElement(['Advertisment','Facebook','Campaign','Event','LinkedIn','Other']),
            'account_id'=>$account->id,
        ];
    }
}
