<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class TutionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'class_name','teacher_instructor','schedule','class_room_location','class_capacity','class_material','start_date','end_date'
            'id'=>Str::uuid(),
            'name'=>$this->faker->name(),
            'teacher_instructor'=>$this->faker->name(),
            'schedule'=>$this->faker->randomElement(['Mon-Fri 10 am - 5pm','Mon-Sat 10 am - 5pm','Sat-Sun 10 am - 5pm']),
            'class_room_location'=>$this->faker->randomElement(['A101','A102','A103','A201','A202','A203','A301','A302','A303']),
            'class_capacity'=>$this->faker->randomElement(['50','70','100','120','200','250','Fast-track 100','Fast-track 200']),
            'class_material'=>$this->faker->randomElement(['Drawing instrument of eng','lab uniform for blood laboratory']),
            'start_date'=>$this->faker->date(),
            'end_date'=>$this->faker->date(),

        ];
    }
}
