<?php

namespace Database\Factories;

use App\Models\Traits\HasUuid;
use App\Models\Tution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class StudentFactory extends Factory
{
    use HasUuid;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'=>Str::uuid(),
            'name'=>$this->faker->name(),
            'gender'=>$this->faker->randomElement(['male','female','other']),
            'dob'=>$this->faker->date(),
            'contact'=>$this->faker->phoneNumber(),
            'guardian_name'=>$this->faker->name(),
            'guardian_contact'=>$this->faker->phoneNumber(),
            'class'=>$this->faker->randomElement(['BE','ME','BA','MA','BCA','MCA','BSC','MSC','B.COM','M.COM','B-tech','M-tech']),
            'year'=>$this->faker->randomElement(['First Year','Second Year','Third Year','Forth Year']),
            'department'=>$this->faker->randomElement(['Engineering','Pharmacy','Medical','Arts','Science']),
            'address'=>$this->faker->address(),

        ];
    }

    public function configure()
    {
        return $this->afterCreating(function ($student) {
            $tutions = Tution::inRandomOrder()->limit(4)->get(); // Adjust the limit as needed

            // $student->tutions()->attach($tutions->id,['id'=> ]);

            foreach ($tutions as $tution) {
                $student->tutions()->attach($tution->id,['id'=> Str::uuid() ]);
            }
        });
    }

}
