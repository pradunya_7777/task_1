<?php

namespace Database\Factories;

use App\Models\Store;
use Illuminate\Support\Str;
use Database\Seeders\StoreSeedeer;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => Str::uuid(),
            'name' => $this->faker->name(),
            'category' => $this->faker->randomElement(['Coldrinks', 'Fruits', 'Grocery', 'Milk Product', 'Other']),
            'brand' => $this->faker->name(),
            'price' => $this->faker->numberBetween(100, 5000),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function ($product) {
            // $stores = Store::select('id')->get('id');
            // $stores = Store::inRandomOrder()->limit(4);
            dd($product);
            $stores = Store::inRandomOrder()->limit(4)->get();
            // $storeIds = $stores->pluck('id')->toArray();
            // dd($stores);

            foreach ($stores as $storeId) {
                // dump($storeId);
                $uuid = Str::uuid();
                $product->stores()->attach($storeId->id, ['id' => $uuid  ]);
            }
        });
    }
}
