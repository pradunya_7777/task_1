<?php

namespace Database\Factories;

use App\Models\Activity;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    protected $model = Activity::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'=>Str::uuid(),
            'subject'=>$this->faker->word(10),
            'relatedTo'=>$this->faker->randomElement(['Account','Contact','Project']),
            'start'=>$this->faker->dateTime(),
            'end'=>$this->faker->dateTimeBetween('-1week','+1week'),
            'taskType'=>$this->faker->word(8),
            'status'=>$this->faker->randomElement(['Assign', 'Inprocess', 'Completed']),
            'description'=>$this->faker->paragraph(1),
            'project_id'=>$this->faker->numberBetween(1,20)
        ];
    }
}
