<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'=>Str::uuid(),
            'name'=>$this->faker->name(),
            'shop_type'=>$this->faker->randomElement(['WholeSaler','Retailer','it Mart','Super Shop','Dairy','Other']),
            'service'=>$this->faker->randomElement(['Walk-IN','Home Delivery','Local-Deal','Other']),
            'city'=>$this->faker->city()
        ];
    }
}
