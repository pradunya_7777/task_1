<?php
return
    [
        'account' => [
            'edit' => [
                [
                    [
                        ['type' => 'label', 'name' => 'Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],

                    ],
                    [
                        ['type' => 'label', 'name' => 'Email'],
                        ['type' => 'email', 'name' => 'email', 'attributes' => ['class' => 'form-control col ']],
                    ],

                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Phone'],
                        ['type' => 'number', 'name' => 'phone', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Partner Code'],
                        ['type' => 'number', 'name' => 'partnercode', 'attributes' => ['class' => 'form-control col']],
                    ]
                ],
                [

                    [
                        ['type' => 'label', 'name' => 'Industry'],
                        ['type' => 'select', 'name' => 'industry', 'attributes' => ['class' => 'form-select', 'placeholder' => 'Select Industry'], 'options' => ['IT' => 'IT', 'Chemical' => 'Chemical', 'Electronics' => 'Electronics', 'Other' => 'Other',]],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Website'],
                        ['type' => 'text', 'name' => 'website', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [

                        ['type' => 'label', 'name' => 'Comment/History'],
                        ['type' => 'textarea', 'name' => 'comment_history', 'attributes' => ['class' => 'form-control col', 'rows' => 3]],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Description/Summary'],
                        ['type' => 'textarea', 'name' => 'description_summary', 'attributes' => ['class' => 'form-control col', 'rows' => 3]],
                    ],
                ],
            ],
        ],
        'contact' => [
            'edit' => [
                [
                    [
                        ['type' => 'hidden', 'name' => 'relation_ship', 'value', 'attributes' => ['class' => 'form-control col', 'hidden']],

                    ]
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Designation'],
                        ['type' => 'text', 'name' => 'designation', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Account'],
                        ['type' => 'selectrelation', 'name' => 'account_id', 'attributes' => ['class' => 'form-select col', 'table' => 'accounts']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Joined Community ?'],
                        ['type' => 'checkbox', 'name' => 'joincommunity', 'attributes' => ['class' => 'form-check-input']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Phone'],
                        ['type' => 'number', 'name' => 'phone', 'attributes' => ['class' => 'form-control col']],

                    ],
                    [
                        ['type' => 'label', 'name' => 'Email'],
                        ['type' => 'text', 'name' => 'email', 'attributes' => ['class' => 'form-control col']],

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Customer Type'],
                        ['type' => 'select', 'name' => 'customer_type', 'attributes' => ['class' => 'form-select'], 'options' => ['Customer' => 'Customer', 'IT Dealer' => 'IT Dealer', 'Competitor' => 'Competitor', 'Currior Company' => 'Currior Company', 'Not Known' => 'Not Known',]]
                    ],
                    [
                        ['type' => 'label', 'name' => 'Customer Position'],
                        ['type' => 'select', 'name' => 'customer_position', 'attributes' => ['class' => 'form-select'], 'options' => ['Junior' => 'Junior', 'Senior' => 'Senior', 'Sales Head' => 'Sales Head', 'Manager' => 'Manager', 'Owner' => 'Owner',]]

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Description'],
                        ['type' => 'textarea', 'name' => 'description', 'attributes' => ['class' => 'form-control col', 'rows' => 3]],

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Comment'],
                        ['type' => 'textarea', 'name' => 'comment', 'attributes' => ['class' => 'form-control col', 'rows' => 3]],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Contact Status'],
                        ['type' => 'radio', 'name' => 'contact_status', 'value' => 'Contacted', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Contacted'],
                        ['type' => 'radio', 'name' => 'contact_status', 'value' => 'Not Yet', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Not Yet'],
                        ['type' => 'radio', 'name' => 'contact_status', 'value' => 'Rejected', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Rejected'],

                    ],
                    [
                        ['type' => 'label', 'name' => 'Follow-up date'],
                        ['type' => 'dateTimeLocal', 'name' => 'followupdate', 'attributes' => ['class' => 'form-control']]
                    ],
                ],

            ]
        ],
        'activity' => [
            'edit' => [
                [
                    [
                        ['type' => 'label', 'name' => 'Subject'],
                        ['type' => 'text', 'name' => 'subject', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Related to'],
                        ['type' => 'text', 'name' => 'relatedTo', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Project'],
                        ['type' => 'selectrelation', 'name' => 'project_id', 'attributes' => ['class' => 'form-select col', 'table' => 'projects']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Description'],
                        ['type' => 'textarea', 'name' => 'description', 'attributes' => ['class' => 'form-control col', 'rows' => 3]]

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Start date'],
                        ['type' => 'dateTimeLocal', 'name' => 'start', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [

                        ['type' => 'label', 'name' => 'End date'],
                        ['type' => 'dateTimeLocal', 'name' => 'end', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Task Type'],
                        ['type' => 'select', 'name' => 'taskType', 'attributes' => ['class' => 'form-control col'], 'options' => ['Coding' => 'Coding', 'Demo/Training' => 'Demo/Training', 'Documentation' => 'Documentation', 'Machine Installation' => 'Machine Installation', 'Other' => 'Other']],
                    ],
                    [

                        ['type' => 'label', 'name' => 'Status'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'Assign', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Assign'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'Inprocess', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Inprocess'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'Completed', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Completed'],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'File Upload'], // Add a label for the file upload field
                        ['type' => 'file', 'name' => 'file', 'attributes' => ['class' => 'form-control col']], // Add the file input field

                    ],
                    [],
                ],
            ]
        ],
        'lead' => [
            'edit' => [
                [
                    [
                        ['type' => 'hidden', 'name' => 'relation_ship', 'value', 'attributes' => ['class' => 'form-control col', 'hidden']],

                    ]
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Account'],
                        ['type' => 'selectrelation', 'name' => 'account_id', 'attributes' => ['class' => 'form-select col', 'table' => 'accounts']]
                    ],

                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Customer Type'],
                        ['type' => 'select', 'name' => 'type', 'attributes' => ['class' => 'form-select col'], 'options' => ['IT Dealer' => 'IT Dealer', 'Customer' => 'Customer', 'Competitor' => 'Competitor', 'Currior Company' => 'Currior Company', 'Not Know' => 'Not Know']]
                    ],
                    [
                        ['type' => 'label', 'name' => ' Status'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'New', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'New'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'Inprocess', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Inprocess'],
                        ['type' => 'radio', 'name' => 'status', 'value' => 'Dead', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Dead'],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Phone'],
                        ['type' => 'number', 'name' => 'phone', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Email'],
                        ['type' => 'text', 'name' => 'email', 'attributes' => ['class' => 'form-control col']],
                    ]
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Product Category'],
                        ['type' => 'select', 'name' => 'product_category', 'attributes' => ['class' => 'form-select col'], 'options' => ['Crm' => 'Crm', 'Taly' => 'Taly']]
                    ],
                    [
                        ['type' => 'label', 'name' => 'No of Users'],
                        ['type' => 'number', 'name' => 'no_of_user', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Industry'],
                        ['type' => 'select', 'name' => 'industry', 'attributes' => ['class' => 'form-select col'], 'options' => ['E-Commerce' => 'E-Commerce', 'Education' => 'Education', 'Finance & Service' => 'Finance & Service', 'Not Known' => 'Not Known', 'Engineering' => 'Engineering', 'Other' => 'Other']]
                    ],
                    [
                        ['type' => 'label', 'name' => 'Lead Source'],
                        ['type' => 'select', 'name' => 'lead_source', 'attributes' => ['class' => 'form-select col'], 'options' => ['Advertisment' => 'Advertisment', 'Campaign' => 'Campaign', 'Event' => 'Event', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn', 'Other' => 'Other']]

                    ],
                ],
            ]
        ],
        'product' => [
            'edit' => [

                [
                    [
                        ['type' => 'hidden', 'name' => 'relation_ship', 'value', 'attributes' => ['class' => 'form-control col', 'hidden']],

                    ]
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Select Stores'],
                        ['type' => 'selectrelation', 'name' => 'store_id[]', 'attributes' => ['class' => 'form-select col', 'table' => 'stores', 'multiple']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Product Category'],
                        ['type' => 'select', 'name' => 'category', 'attributes' => ['class' => 'form-select col'], 'options' => ['Coldrinks' => 'Coldrinks', 'Fruits' => 'Fruits', 'Grocery' => 'Grocery', 'Milk Product' => 'Milk Product', 'Other' => 'Other']]

                    ],
                    [
                        ['type' => 'label', 'name' => 'Brand Name'],
                        ['type' => 'text', 'name' => 'brand', 'attributes' => ['class' => 'form-control col']],

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Product Price'],
                        ['type' => 'number', 'name' => 'price', 'attributes' => ['class' => 'form-control col']]

                    ],
                    [],

                ],
            ]
        ],
        'store' => [
            'edit' => [
                [
                    [
                        ['type' => 'label', 'name' => 'Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Product Name'],
                        ['type' => 'selectrelation', 'name' => 'product_id[]', 'attributes' => ['class' => 'form-select col', 'table' => 'products', 'multiple']],

                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Type of Shop'],
                        ['type' => 'select', 'name' => 'shop_type', 'attributes' => ['class' => 'form-select col'], 'options' => ['WholeSaler' => 'WholeSaler', 'Retailer' => 'Retailer', 'Fruit Mart' => 'Fruit Mart', 'Super Shop' => 'Super Shop', 'Dairy' => 'Dairy', 'Other' => 'Other']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Service'],
                        ['type' => 'select', 'name' => 'service', 'attributes' => ['class' => 'form-select col'], 'options' => ['Walk-IN' => 'Walk-IN', 'Home Delivery' => 'Home Delivery', 'Local-Deal' => 'Local-Deal', 'Other' => 'Other']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'City Name'],
                        ['type' => 'text', 'name' => 'city', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [],
                ],
            ]
        ],
        'student' => [
            'edit' => [
                [
                    [
                        ['type' => 'hidden', 'name' => 'relation_ship', 'value', 'attributes' => ['class' => 'form-control col', 'hidden']],

                    ]
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Student Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Gender'],
                        ['type' => 'radio', 'name' => 'gender', 'value' => 'Male', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Male'],
                        ['type' => 'radio', 'name' => 'gender', 'value' => 'Female', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Female'],
                        ['type' => 'radio', 'name' => 'gender', 'value' => 'Other', 'attributes' => ['class' => 'form-check-input']],
                        ['type' => 'label', 'name' => 'Other'],
                    ],

                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Tution'],
                        ['type' => 'selectrelation', 'name' => 'tution_id[]', 'attributes' => ['class' => 'form-select col', 'table' => 'tutions', 'multiple']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Student Address'],
                        ['type' => 'textarea', 'name' => 'address', 'attributes' => ['class' => 'form-control', 'rows' => 3]]
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Date of birth'],
                        ['type' => 'date', 'name' => 'dob', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Contact'],
                        ['type' => 'number', 'name' => 'contact', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Guardian Name'],
                        ['type' => 'text', 'name' => 'guardian_name', 'attributes' => ['class' => 'form-control']]
                    ],
                    [
                        ['type' => 'label', 'name' => 'Guardian Contact'],
                        ['type' => 'number', 'name' => 'guardian_contact', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Class Name'],
                        ['type' => 'select', 'name' => 'class', 'attributes' => ['class' => 'form-select col'], 'options' => ['Bcom' => 'Bcom', 'Mcom' => 'Mcom', 'BA' => 'BA', 'MA' => 'MA', 'BSC' => 'BSC', 'MSC' => 'MSC', 'BCA' => 'BCA', 'MCA' => 'MCA', 'BE' => 'BE', 'ME' => 'ME', 'B-Tec' => 'B-Tec', 'M-Tec' => 'M-tech']]
                    ],
                    [
                        ['type' => 'label', 'name' => 'Year'],
                        ['type' => 'select', 'name' => 'year', 'attributes' => ['class' => 'form-select col'], 'options' => ['First Year' => 'First Year', 'Second Year' => 'Second Year', 'Third Year' => 'Third Year', 'Forth Year' => 'Forth Year', 'Fifth Year' => 'Fifth Year', 'Sixth Year' => 'Sixth Year']]
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Department'],
                        ['type' => 'select', 'name' => 'department', 'attributes' => ['class' => 'form-select col'], 'options' => ['Arts' => 'Arts', 'Commerce' => 'Commerce', 'Science' => 'Science', 'Engineering' => 'Engineering', 'Pharmacy' => 'Pharmacy', 'Medical' => 'Medical', 'Other' => 'Other']]
                    ],
                    []
                ]
            ]
        ],
        'tution' => [
            'edit' => [
                [
                    [
                        ['type' => 'label', 'name' => 'Class Name'],
                        ['type' => 'text', 'name' => 'name', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Teacher/Instructor'],
                        ['type' => 'text', 'name' => 'teacher_instructor', 'attributes' => ['class' => 'form-control col']],
                    ],

                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Class Room Location'],
                        ['type' => 'text', 'name' => 'class_room_location', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Schedule'],
                        ['type' => 'text', 'name' => 'schedule', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Class Material'],
                        ['type' => 'text', 'name' => 'class_material', 'attributes' => ['class' => 'form-control col']],
                    ],
                    [
                        ['type' => 'label', 'name' => 'Class Capacity'],
                        ['type' => 'number', 'name' => 'class_capacity', 'attributes' => ['class' => 'form-control col']],
                    ],
                ],
                [
                    [
                        ['type' => 'label', 'name' => 'Start Date'],
                        ['type' => 'date', 'name' => 'start_date', 'attributes' => ['class' => 'form-control col']]
                    ],
                    [
                        ['type' => 'label', 'name' => 'End Date'],
                        ['type' => 'date', 'name' => 'end_date', 'attributes' => ['class' => 'form-control col']]
                    ],
                ],
            ]
        ],
        'relationship' => [
            'Product' => [
                'stores' => [
                    'relationshipName' => 'store_id'
                ],
                "task" => [
                    'relationshipName' => 'task_id',
                ],
            ],
            'Student' => [
                'tutions' => [
                    'relationshipName' => 'tution_id'
                ],
                "task" => [
                    'relationshipName' => 'task_id',
                ],
            ],
            'Store' => [
                'products' => [
                    'relationshipName' => 'product_id'
                ],
            ],
            'Contact'=>[
                'accounts'=>[
                    'relationshipName'=>'account_id'
                ],
            ],
            'Lead'=>[
                'accounts'=>[
                    'relationshipName'=>'account_id'
                ],
            ],
        ],





        // 'products' => [
        //     'relationshipName' => [
        //         'product_id',
        //     ]
        // ],
        // 'students' => [
        //     'relationshipName' => [
        //         'student_id',
        //     ]
        // ],
        // 'tutions' => [
        //     'relationshipName' => [
        //         'tution_id',
        //     ]
        // ],


    ];
