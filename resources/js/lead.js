$(function () {
    $("#table").DataTable({
        "scrollX": true,
        "scrollY":"370px",
        "scrollCollapse": true,

        ajax: "/leads/fetch",
        type: "GET",
        columns: [
            { data: "name" ,
            render: function (data, type, row) {

                return '<a href="/leads/'+ row.id +'">'+ row.name + '</a>';
            return data;
            }
             },
            { data: "type" },
            { data: "status" },
            { data: "phone" },
            { data: "email" },
            { data: "product_category" },
            { data: "no_of_user" },
            { data: "industry" },
            { data: "lead_source" },
            { data: "name",
                render: function (data, type, row) {
                    return (
                        '<a href="/account/show/' +
                        row.account_id +
                        '">' +
                        row.accounts_name +
                        "</a>"
                    );

                    return data;
                },
                },
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
