$(function () {
    $("#table").DataTable({
        "scrollX": true,
        "scrollY":"370px",
        "scrollCollapse": true,
        ajax: "/contacts/fetch",
        type: "GET",
        columns: [
            {
                data: "name",
                render: function (data, type, row) {
                    return (
                        '<a href="/contacts/show/' +
                        row.id +
                        '">' +
                        row.name +
                        "</a>"
                    );

                    return data;
                },
            },
            { data: "designation" },
            { data: "email" },
            { data: "phone" },
            { data: "customer_type" },
            { data: "customer_position" },
            { data: "description" },
            { data: "comment" },
            { data: "contact_status" },
            { data: "followupdate" },
            {
                data: "id", // Assuming 'id'
                render: function (data, type, row) {
                    if (type === "display") {
                        const joincommunityvalue = row.joincommunity
                            ? "checked"
                            : "";
                        return (
                            '<input type="checkbox" id="check' +
                            data +
                            '" class="joincommunity" ' +
                            joincommunityvalue +
                            ' name="joincommunity">'
                        );
                    }
                    return data;
                },
            },
            {
            data: "name",
            render: function (data, type, row) {
                return (
                    '<a href="/account/show/' +
                    row.account_id +
                    '">' +
                    row.accounts_name +
                    "</a>"
                );

                return data;
            },
            },
            // {data: "accounts.name AS 'Accounts_name'"},
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });

    // $.ajax({
    //     url: "/contacts/fetch", // Replace with the actual route for fetching contacts
    //     type: "GET",
    //     processing: true,
    //     serverSide: true,

    //     success: function (data) {
    //         // Initialize an empty HTML string for the table rows
    //         var tableRows = "";
    //         $.each(data, function (index, contact) {
    //             // Create a new row for each contact
    //             var joincommunityvalue =
    //                 contact.joincommunity == "yes" ? "checked" : "";

    //             tableRows +=
    //                 "<tr><td><a href='/contacts/show/" +
    //                 contact.id +
    //                 "'>" +
    //                 contact.name +
    //                 " </a></td><td>" +
    //                 contact.designation +
    //                 "</td><td>" +
    //                 contact.email +
    //                 "</td><td>" +
    //                 contact.phone +
    //                 "</td><td>" +
    //                 contact.customer_type +
    //                 "</td><td>" +
    //                 contact.customer_position +
    //                 "</td><td>" +
    //                 contact.description +
    //                 "</td><td>" +
    //                 contact.comment +
    //                 "</td><td>" +
    //                 contact.contact_status +
    //                 "</td><td>" +
    //                 contact.followupdate +
    //                 "</td><td> " +
    //                 '<input type="checkbox" id="check' +
    //                 contact.id +
    //                 '" class="joincommunity" ' +
    //                 joincommunityvalue +
    //                 ' name="joincommunity">' +
    //                 "</td></tr>";
    //         });

    //         // Append the rows to the contact-list tbody
    //         $("#contact-list").html(tableRows);
    //     },
    // });

    // console.log("dd");
    // $("#contact-list").on("click", function () {
    //     console.log($(this).children().find(".joincommunity"));
    // });

    // $('input').on('click',' change', function () {
    //     console.log('test');
    // });
    // $('.joincommunity').on('click', function () {
    //     console.log(this);
    // if ($(this).is(":checked")) {
    //     // Checked
    //     console.log('checked');
    // } else {
    //     // Unchecked
    //     console.log('unchecked');
    // }
    // });
    // console.log("dd");
});
