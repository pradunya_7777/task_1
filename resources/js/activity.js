$(function () {
    $("#table").DataTable({
        "scrollX": true,
        "scrollY": "370px",
        "scrollCollapse": true,

        ajax: "/activities/fetch",
        type: "GET",
        columns: [{ data: "subject" ,
            render: function (data, type, row) {

                return '<a href="/activities/'+ row.id +'">'+ row.subject + '</a>';

            return data;
            }
             },
            { data: "relatedTo" },
            { data: "start" },
            { data: "end" },
            { data: "taskType" },
            { data: "status" },
            { data: "description" },
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
