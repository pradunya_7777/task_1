$(function () {
    function makeAJAXRequest(childId,ParentId,Model,Function_Name){

      $.ajax({
        url: "/products/detachproduct",
        type: 'GET',
        datatype: 'json',
        data: {
            child_Id: childId,
            parent_Id: ParentId,
            model:Model,
            function_name: Function_Name,
        },
        success: function (response) {
            console.log(response);
        },
      });
    }

    $(".myButton").on("click", function () {
        var ParentId = $("#hidden").val();

        var Model = $("#model").val();

        var Function_Name = $("#function_name").val();

        // console.log("Product-Id",ParentId);
            var childId = $(this).data("child-id");
            // console.log("Store-Id:",childId);
        makeAJAXRequest(childId,ParentId,Model,Function_Name);
        location.reload(true);
    });


});

