$(function () {
    $("#table").DataTable({

        // "scrollX": true,
        "scrollY":"370px",
        "scrollCollapse": true,

        ajax: "/tutions/fetch",
        type: "GET",
        columns: [
            { data: "name",
            render: function (data, type, row) {
                return (
                    '<a href="/tutions/' +
                    row.id +
                    '">' +
                    row.name +
                    "</a>"
                );

                return data;
            },
        },
            { data: "teacher_instructor" },
            { data: "class_room_location" },
            { data: "class_capacity" },
            { data: "class_material" },
            { data: "start_date" },

        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
// 'class_name','teacher_instructor','schedule','class_room_location','class_capacity','class_material','start_date','end_date'
