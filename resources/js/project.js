$(function () {
    $("#table").DataTable({

        "scrollY":"370px",
        "scrollCollapse": true,


        ajax: "/projects/fetch",
        type: "GET",
        columns: [
            { data: "name",
            render: function (data, type, row) {
                return (
                    '<a href="/projects/' +
                    row.id +
                    '">' +
                    row.name +
                    "</a>"
                );

                return data;
            },
        },
            { data: "priority" },
            { data: "project_type" },
            { data: "start_date" },
            { data: "due_date" }
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
