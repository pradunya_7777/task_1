$(function() {
    // Handle form submission
    $('#update-form').on('submit', function() {
         // Prevent the default form submission

        // Serialize the form data
        var formData = $(this).serialize();

        // Make an AJAX request
        $.ajax({
            url: $(this).attr('action'), // Get the action URL from the form's action attribute
            type: 'POST',
            data: formData,
            success: function(response) {

            }
        });
    });
});
