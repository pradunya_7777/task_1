$(function () {
    $("#table").DataTable({

        "scrollY":"370px",

        ajax: "/products/fetch",
        type: "GET",
        columns: [
            { data: "name",
            render: function (data, type, row) {

                return '<a href="/products/'+ row.id +'">'+ row.name + '</a>';
            return data;
            }
            },
            { data:'category' },
            { data:'brand' },
            { data:'price' },
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
