$(function () {
    $("#table").DataTable({

        // "scrollX": true,
        "scrollY":"370px",
        "scrollCollapse": true,

        ajax: "/students/fetch",
        type: "GET",
        columns: [
            { data: "name",
            render: function (data, type, row) {
                return (
                    '<a href="/students/' +
                    row.id +
                    '">' +
                    row.name +
                    "</a>"
                );

                return data;
            },

        },
            { data: "gender" },
            { data: "dob" },
            { data: "contact" },
            { data: "class" },
            { data: "year" },
            { data: "department" },

        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
//'student_name','gender','dob','contact','guardian_name','guardian_contact','class','year','department','address'
