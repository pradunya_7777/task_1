$(function () {
    $("#table").DataTable({
        "scrollY":"370px",


        ajax: "/stores/fetch",
        type: "GET",
        columns: [
            { data: "name",
            render: function (data, type, row) {

                return '<a href="/stores/'+ row.id +'">'+ row.name + '</a>';
            return data;
            }
        },
        {data:"shop_type"},
        {data:"service"},
        {data:"city"},
        ],
        processing: true,
        serverSide: true,
    });
    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });
});
