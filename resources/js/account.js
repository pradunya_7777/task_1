$(function () {
  var table=$("#table").DataTable({

        "scrollX": true,
        "scrollY": "370px",
        "scrollCollapse": true,
        "columnDefs": [
            { "width": "100px", "targets": 6 },
          ],
        "fixedColumns": true,
        ajax: "/account/fetch",
        type: "GET",
        // "order": [[0,'asc']],
        columns: [
            { data: "name" ,
            render: function (data, type, row) {

                return '<a href="/account/show/'+ row.id +'">'+ row.name + '</a>';

            return data;
            }
            },
            { data: "email" ,
            render: function (data, type, row) {

                return '<a href="mailto:'+ row.email+'">'+ row.email + '</a>';

            return data;
            }
            },
            // { data: "email" },
            { data: "phone" },
            { data: "partnercode" },
            { data: "industry" },
            { data: "website" },
            { data: "comment_history" },
            { data: "description_summary" },
        ],


        processing: true,
        serverSide: true,

    });

    $('.table_filter').on( 'keyup', function () {
        table.search( this.value ).draw();
    });

    // $.ajax({
    //     url: "/account/fetch", // Replace with the actual route for fetching contacts
    //     type: "GET",
    //     processing: true,
    //     serverSide: true,

    //     success: function (data) {
    //         // Initialize an empty HTML string for the table rows
    //         var tableRows = "";
    //         $.each(data, function (index, account) {
    //             // Create a new row for each contact

    //             tableRows +=
    //                 "<tr><td>"+
    //                 account.name +
    //                 " </a></td><td>" +
    //                 account.email +
    //                 "</td><td>" +
    //                 account.phone +
    //                 "</td><td>" +
    //                 account.partnercode +
    //                 "</td><td>" +
    //                 account.industry +
    //                 "</td><td>" +
    //                 account.website +
    //                 "</td><td>" +
    //                 account.comment_history +
    //                 "</td><td>" +
    //                 account.description_summary +
    //                 "</td></tr>";
    //         });

    //         // Append the rows to the contact-list tbody
    //         $("#account-list").html(tableRows);
    //     },
    // });

    // $("#account-list").on("click", function () {
    //     console.log($(this).children().find(".joincommunity"));
    // });
});
