@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="container">
                        <h4 class="mt-3">Edit Tution</h4>
                        {{  Form::model($tution, ['route' => ['tutions.update', $tution->id]],['class'=>'mt-2','id'=>'update-form'])  }}
                            @method('PATCH')
                            {{ Form::token() }}
                            @include('tutions.form')

                            <div class="mt-4 mb-4">

                                <button type="submit" id="update-button" class="btn btn-outline-primary mx-5">Save</button>

                                <a href='{{ route('tutions.index') }}'>
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
