@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container ">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <td>Tution Name</td>
                                    <td>{{ $tution->name }}</td>
                                </tr>
                                <tr>
                                    <td>Teacher/Instructor</td>
                                    <td>{{ $tution->teacher_instructor }}</td>
                                </tr>
                                 <tr>
                                    <td>Class Schedule</td>
                                    <td>{{ $tution->schedule }}</td>
                                </tr>
                                 <tr>
                                    <td>Class Room Location</td>
                                    <td>{{ $tution->class_room_location }}</td>
                                </tr>
                                <tr>
                                    <td>Class Capicity</td>
                                    <td>{{ $tution->class_capacity }}</td>
                                </tr>
                                 <tr>
                                    <td>Material/Eqipment</td>
                                    <td>{{ $tution->class_material }}</td>
                                </tr>
                                 <tr>
                                    <td>Start Date</td>
                                    <td>{{ $tution->start_date }}</td>
                                </tr>
                                 <tr>
                                    <td>End Date</td>
                                    <td>{{ $tution->end_date }}</td>
                                </tr>

                            </tbody>
                        </table>

                            <a href="{{ route('tutions.edit',['tution'=>$tution->id]) }}">
                                <button class="btn btn-primary mx-2 mb-3">Edit</button>
                            </a>
                            <a href="{{ route('tutions.index') }}">
                                <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                            </a>
                            <a>
                            <form action="{{ route('tutions.destroy', ['tution' => $tution->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger mx-2 mb-3">Delete</button>
                            </form>
                        </a>

                            <table class="table table-borderless mt-2">

                                <tbody>

                                    <tr><td><h5>Additional Details Here please</h5></td></tr>

                                </tbody>
                            </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
