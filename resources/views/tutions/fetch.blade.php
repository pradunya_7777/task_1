@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="container"style="width: 100%; overflow-x: auto;">
                        <a href="{{ route('stores.create') }}">Create store</a>
                        <table class="table" >
                            <thead class="table-dark">
                                <th>Store ID</th>
                                <th>Name</th>

                            </thead>
                            <tbody>
                                @foreach ($stores as $store)
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
