


@foreach (@config('layouts.tution.edit') as $fields)

    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach





{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('class_name', 'Class Name') }} @error('class_name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('class_name', null, ['class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{ Form::label('teacher_instructor', 'Teacher/Instructor') }}
        {{ Form::text('teacher_instructor', null, ['class' => 'form-control']) }}
    </div>
</div>


<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("schedule", "Schedule")}}
        {{ Form::text('schedule', null ,  $attributes = ['class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label("class_room_location", "Class Room Location")}}
        {{ Form::text('class_room_location', null , $attributes = ['class' => 'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("class_capacity", "Class Capacity")}}
        {{ Form::number('class_capacity', null,['class' => 'form-control'] ) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label("class_material", "Class Material")}}
        {{ Form::text('class_material', null , $attributes = ['class' => 'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">

        {{ Form::label('start_date', 'Start Date') }}
        {{ Form::date('start_date', null,['class' => 'form-control'] ) }}

    </div>
    <div class="form-group col-md-4">

        {{ Form::label('end_Date', 'End Date ') }}
        {{ Form::date('end_date', null,['class' => 'form-control'] ) }}

    </div>

</div> --}}


