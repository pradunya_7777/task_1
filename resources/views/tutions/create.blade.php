@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container">
                        <h4 class="mt-3">Tution</h4>
                        {{ Form::open(['route' => ['tutions.store']]) }}
                        {{ Form::token() }}
                            @include('tutions.form')

                            <div class="mt-4 mb-2">

                                <button type="submit" class="btn btn-outline-primary mx-5">Save</button>
                                <a href="{{ route('tutions.index') }}">
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
