@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="container"style="width: 100%; overflow-x: auto;">
                        <a href="{{ route('products.create') }}">Create lead</a>
                        <table class="table table-striped" >
                            <thead class="table-dark">
                                {{-- <th>Product ID</th> --}}
                                <th>Name</th>

                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                <tr>
                                    {{-- <td></td> --}}
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
