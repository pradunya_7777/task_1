@extends('layouts.app')

@section('content')

    <script src="{{ asset('js/productdetach.js') }}"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <td>Product Name</td>
                                    <td>{{ $product->name }}</td>
                                </tr>
                                <tr>
                                    <td>Product Category</td>
                                    <td>{{ $product->category }}</td>
                                </tr>
                                <tr>
                                    <td>Product Brand</td>
                                    <td>{{ $product->brand }}</td>
                                </tr>
                                <tr>
                                    <td>Product Price</td>
                                    <td>{{ $product->price }}</td>
                                </tr>

                            </tbody>
                        </table>


                        <input type="hidden" id="hidden" name="relation_ship" value="{{ $product->id }}" />
                        <input type="hidden" id="model" name="model" value="Product" />
                        <input type="hidden" id="function_name" name="function_name" value="stores" />
                        <a href="{{ route('products.edit', ['product' => $product->id]) }}">
                            <button class="btn btn-primary mx-2 mb-3">Edit</button>
                        </a>
                        <a href="{{ route('products.index') }}">
                            <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                        </a>
                        <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger mx-2 mb-3">Delete</button>
                        </form>

                        <table class="table table-borderless mt-2">

                            <tbody>
                                <tr>
                                    <td>
                                        <h3>All Store</h3>
                                    </td>
                                </tr>
                                @foreach ($stores as $store)
                                    <tr>
                                        <td>Store Name</td>
                                        <td>
                                        {{-- <td><a href="{{ route('stores.show', ['store' => $store->id]) }}" class="store-link">{{ $store->name }}</a> --}}
                                            <a href="{{ route('stores.show', ['store' => $store->id]) }}" class="store-link">{{ $store->name }}</a>

                                        </td>
                                        <td>

                                        <button class="myButton btn btn-danger" id="myButton" data-child-id="{{ $store->id }}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                          </svg></button>



                                        </td>
                                    </tr>
                                @endforeach



                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
