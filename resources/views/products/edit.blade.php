@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="container">
                        {{-- <form class="mb-3" id="update-form" method="POST" action="{{ route('activities.update', ['activity' => $activity->id]) }}"> --}}
                            {{  Form::model($product, ['route' => ['products.update', $product->id]],['class'=>'mt-2','id'=>'update-form'])  }}
                            @method('PATCH')
                            {{ Form::token() }}
                            @include('products.form')

                            <div class="mt-4 mb-4">

                                <button type="submit" id="update-button" class="btn btn-outline-primary mx-5">Save</button>

                                <a href='{{ route('products.index') }}'>
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
