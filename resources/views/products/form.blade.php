
@foreach (@config('layouts.product.edit') as $fields)

    <div class="row">
        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach



{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('name', 'Product Name') }} @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('name', null, ['class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{  Form::label("stores", "Select Store")}}
        {{ Form::select('stores', $stores, null, ['class' => 'form-select', 'multiple'  ]) }}
    </div>
</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("category", "Product Category")}}
        {{ Form::select('category', ['Coldrinks' => 'Coldrinks', 'Fruits' => 'Fruits','Grocery'=>'Grocery','Milk Product'=>'Milk Product','Other'=>'Other'], null , ['placeholder' => 'Select Product Category','class'=>'form-control']) }}

    </div>

    <div class="form-group col-md-4">

    {{ Form::label('brand', 'Brand Name') }}
    {{ Form::text('brand', null, ['class' => 'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("price", "Product price")}}
        {{ Form::number('price', null, ['class' => 'form-control']) }}
    </div>

</div> --}}
