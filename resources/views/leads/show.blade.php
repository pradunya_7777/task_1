@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container ">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <td>Lead Name</td>
                                    <td>{{ $lead->name }}</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>{{ $lead->type }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{{ $lead->status }}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>{{ $lead->phone }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $lead->email }}</td>
                                </tr>
                                <tr>
                                    <td>Product Category</td>
                                    <td>{{ $lead->product_category }}</td>
                                </tr>
                                <tr>
                                    <td>No of Users</td>
                                    <td>{{ $lead->no_of_user }}</td>
                                </tr>
                                <tr>
                                    <td>Industry</td>
                                    <td>{{ $lead->industry }}</td>
                                </tr>
                                <tr>
                                    <td>Lead Source</td>
                                    <td>{{ $lead->lead_source }}</td>
                                </tr>

                            </tbody>
                        </table>



                            <a href="{{ route('leads.edit',['lead'=>$lead->id]) }}">
                                <button class="btn btn-primary mx-2 mb-3">Edit</button>
                            </a>
                            <a href="{{ route('leads.index') }}">
                                <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                            </a>
                            <a href="{{ route('leads.destroy',['lead'=>$lead->id]) }}">
                                <button class="btn btn-danger mx-2 mb-3">Del</button>
                            </a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
