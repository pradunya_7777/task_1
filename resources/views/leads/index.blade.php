@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/lead.js') }}"></script>
    <script src="{{ asset('js/leadupdate.js') }}"></script>
    <style>
        .table-container {
            width: 100%;
            max-height: 500px;
            /* overflow: auto; */
        }
        th, td { white-space: nowrap;}
        tr { height: 50px;}


    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('leads.create') }}" class="px-3 py-3">Create Lead</a>
                    <div class="table-container">
                        <table id="table" class="table hover">
                            <thead class="table-dark">
                                <th>Lead Name</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Product Category</th>
                                <th>No of users</th>
                                <th>Industry</th>
                                <th>Lead Source</th>
                                <th>Account Name</th>


                            </thead>
                            <tbody id="contact-list">

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
