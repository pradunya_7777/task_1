@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="container"style="width: 100%; overflow-x: auto;">
                        <a href="{{ route('leads.create') }}">Create lead</a>
                        <table class="table" >
                            <thead class="table-dark">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Customer Type</th>
                                <th>Customer Position</th>
                                <th>Description</th>
                                <th>Comment</th>
                                <th>Contact Status</th>

                            </thead>
                            <tbody>
                                @foreach ($contacts as $contact)
                                <tr>
                                    <td></td>
                                    <td><td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
