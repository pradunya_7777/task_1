@foreach (@config('layouts.lead.edit') as $fields)

    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach


{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('lead_name', 'Lead Name') }} @error('lead_name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('lead_name', null, ['class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4 ">
        {{ Form::label('account_id ', 'Account Name') }}@error('account_id')
        <div class="text-danger">{{ $message }}</div>
    @enderror
        {{ Form::select('account_id', $account, null, ['placeholder' => 'Select Account', 'class' => 'form-control']) }}

    </div>
</div>

<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('type', 'Type') }}
        {{ Form::select('type', ['IT Dealer' => 'IT Dealer', 'Customer' => 'Customer', 'Competitor' => 'Competitor', 'Currior Company' => 'Currior Company', 'Not Know' => 'Not Know'], null, ['placeholder' => 'Select Customer type', 'class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{ Form::label('status', 'Status') }}
        <div class="form-check-group">
            {{ Form::radio('status', 'New'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'New') }}

            {{ Form::radio('status', 'In Process'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'In Process') }}


            {{ Form::radio('status', 'Dead'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'Dead') }}
        </div>
</div>

<div class="row pt-3">


    <div class="form-group col-md-4">
        {{ Form::label('phone', 'Phone') }}@error('phone')
        <div class="text-danger">{{ $message }}</div>
    @enderror
        {{ Form::text('phone', null, ['class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', null, ['class' => 'form-control']) }}

    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('product_category', 'Product Category') }}
        {{ Form::select('product_category', ['Crm' => 'Crm', 'Taly' => 'Taly'], null, ['placeholder' => 'Select Product Category', 'class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label('no_of_user', 'No of users') }}
        {{ Form::number('no_of_user', null, ['class' => 'form-control']) }}

    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('industry', 'Industry') }}
        {{ Form::select('industry', ['E-Commerce' => 'E-Commerce', 'Education' => 'Education', 'Finance & Service' => 'Finance & Service', 'Not Known' => 'Not Known', 'Engineering' => 'Engineering', 'Other' => 'Other'], null, ['placeholder' => 'Select Industry', 'class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label('lead_source', 'Lead Source') }}@error('lead_source')
        <div class="text-danger">{{ $message }}</div>
    @enderror
        {{ Form::select('lead_source', ['Advertisment' => 'Advertisment', 'Campaign' => 'Campaign', 'Event' => 'Event', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn', 'Other' => 'Other'], null, ['placeholder' => 'Select Lead Source', 'class' => 'form-control']) }}
    </div>
</div> --}}
