<div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('name', 'Product Name') }} @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('name', null, ['class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{  Form::label("implementation_status", "Implimentation Status")}}
        {{ Form::select('implementation_status', ['New'=>'New','Assign'=>'Assign','Inprocess'=>'Inprocess','Code Review'=>'Code Review','Merge Pending'=>'Merge Pending','Stage Realese'=>'Stage Realese','Completed'=>'Completed','On hold'=>'On hold','Customer Side Pending'=>'Customer Side Pending'], null, ['placeholder' => 'Select Implementation type','class' => 'form-select']) }}
    </div>


</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("priority", "Priority")}}@error('priority')
        <div class="text-danger">{{ $message }}</div>
    @enderror
        {{ Form::select('priority', ['Hign' => 'Hign', 'Medium' => 'Medium','Low'=>'Low'], null, ['placeholder' => 'Select Priority','class' => 'form-select']) }}

    </div>

    <div class="form-group col-md-4">

    {{ Form::label('project_type', 'Project Type') }}
    {{ Form::select('project_type',['Addon' => 'Addon', 'Fussion' => 'Fussion','Mobile Application'=>'Mobile Application', 'POC' => 'POC', 'R & D' => 'R & D', 'Free CRM' => 'Free CRM', 'Template' => 'Template', 'Optimize' => 'Optimize'], null , ['placeholder' => 'Select Project Type','class'=>'form-select']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("start_date", "Start Date")}}@error('start_date')
        <div class="text-danger">{{ $message }}</div>
    @enderror
        {{ Form::date('start_date', null,['class' => 'form-control date'] ) }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label("due_date", "Due Date")}}
        {{ Form::date('due_date', null,['class' => 'form-control date'] ) }}
    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("product", "Product")}}
        {{ Form::select('product', ['Cloud Telephony' => 'Cloud Telephony','CRM' => 'CRM','CRM book Subcription'=>'CRM book Subcription','Enjay Latitude Lite' => 'Enjay Latitude Lite','Enjay Ensight' => 'Enjay Ensight','Enjay out mails' => 'Enjay out mails'], null , ['placeholder' => 'Select Priority','class'=>'form-select']) }}

    </div>

    <div class="form-group col-md-4">

    {{ Form::label('project_hosting_type', 'Project Hosting Type') }}
    {{ Form::select('project_hosting_type',['Enjay Cloud' => 'Enjay Cloud', 'On site' => 'On site','Internal Project'=>'Internal Project', 'Client Cloud' => 'Client Cloud'], null , ['placeholder' => 'Select Hosting Type','class'=>'form-select']) }}

    </div>

</div>
