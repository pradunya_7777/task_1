@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/project.js') }}"></script>
    <style>
        .table-container {
            width: 100%;
            max-height: 500px;
            /* overflow: auto; */
        }
        th, td { white-space: nowrap;}
        tr { height: 50px;}


    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('projects.create') }}" class="px-3 py-3">Add Project</a>
                    <div class="table-container">
                        <table id="table" class="table hover">
                            <thead class="table-dark">
                                <th>Project Name</th>
                                <th>Priority</th>
                                <th>project Type</th>
                                <th>Start Date</th>
                                <th>Due Date</th>

                            </thead>
                            <tbody id="contact-list">

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
