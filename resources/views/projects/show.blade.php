@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container">

                        <h4 class="mt-3">Project Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>


                                <tr>
                                    <td>Project Name</td>
                                    <td>{{ $project->name }}</td>
                                </tr>
                                <tr>
                                    <td>Implementation Status</td>
                                    <td>{{ $project->implementation_status }}</td>
                                </tr>
                                <tr>
                                    <td>Priority</td>
                                    <td>{{ $project->priority }}</td>
                                </tr>
                                <tr>
                                    <td>Project Type</td>
                                    <td>{{ $project->project_type }}</td>
                                </tr>
                                <tr>
                                    <td>Start Date</td>
                                    <td>{{ $project->start_date }}</td>
                                </tr>
                                <tr>
                                    <td>Due date</td>
                                    <td>{{ $project->due_date }}</td>
                                </tr>
                                <tr>
                                    <td>Product</td>
                                    <td>{{ $project->product }}</td>
                                </tr>                                <tr>
                                    <td>Project Hosting Type</td>
                                    <td>{{ $project->project_hosting_type }}</td>
                                </tr>

                            </tbody>
                        </table>



                        <a href="{{ route('projects.edit', ['project' => $project->id]) }}">
                            <button class="btn btn-primary mx-2 mb-3">Edit</button>
                        </a>
                        <a href="{{ route('projects.index') }}">
                            <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                        </a>
                        <a href="{{ route('projects.destroy', ['project' => $project->id]) }}">
                            <button class="btn btn-danger mx-2 mb-3">Delete</button>
                        </a>

                        <table class="table table-borderless mt-2">

                            <tbody>
                                <tr>
                                    <td><h3>All Activities</h3></td>
                                </tr>
                                {{-- @dd($activities) --}}
                                @foreach ($activities as $activity )
                                <tr>
                                    <td>Name</td>
                                    <td><a href="{{ route('activities.show',['activity'=>$activity->id]) }}">{{ $activity->subject }}</a></td>

                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
