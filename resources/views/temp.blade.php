
<div class="row pt-3">
    <div class="form-group col-md-4">
        @include('fields.label',['name'=>'Phone']) @error('phone')<div class="text-danger">{{ $message }}</div>@enderror
        {{-- <input class="form-control"type="tel" name="phone" id=""value="{{ isset($account->phone)?$account->phone:'' }}"> --}}
        {{-- {{ Form::number('phone', null , $attributes = ['class' => 'form-control']) }} --}}
        @include('fields.number',['name'=>'phone','attributes'=>['class'=>'form-control']])
    </div>

    <div class="form-group col-md-4">
        @include('fields.label',['name'=>'Partner Code'])
        {{-- <input class="form-control" type="number" name="partnercode" id="" value="{{ isset($account->partnercode)?$account->partnercode:'' }}"> --}}
        {{-- {{ Form::number('partnercode', null ,  $attributes = ['class' => 'form-control','placeholder'=>'Partner code  accept only number']) }} --}}
        @include('fields.number',['name'=>'partnercode','attributes'=>['class'=>'form-control','placeholder'=>'Partner code  accept only number']])


    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        @include('fields.label',['name'=>'Industry'])  @error('industry')<div class="text-danger">{{ $message }}</div> @enderror
    {{-- {{ Form::select('industry', ['IT' => 'IT', 'Chemical' => 'Chemical','Advertisment'=>'Advertisment'], null , ['placeholder' => 'select Industry','class'=>'form-control']) }} --}}
    @include('fields.select', ['name' => 'industry', 'options' => ['IT' => 'IT', 'Chemical' => 'Chemical', 'Advertisment' => 'Advertisment','Other' => 'Other'], 'attributes' => ['placeholder' => 'select Industry', 'class' => 'form-select']])
</div>

<div class="form-group col-md-4">
    @include('fields.label',['name'=>'Website'])
    {{-- <input class="form-control" type="url" name="website" id=""value="{{ isset($account->website) ? $account->website : '' }}"> --}}
    {{-- {{ Form::text('website', null ,  $attributes = ['class' => 'form-control']) }} --}}
    @include('fields.text',['name'=> "website",'attributes'=>['class'=>'form-control']])

</div>
</div>
<div class="row pt-3">
<div class="form-group col-md-4">
    {{-- {{ Form::label('comment_history', 'Comment/History') }} --}}
    @include('fields.label',['name'=>'Comment/History'])

    {{-- <textarea name="comment_history" id="" class="form-control" cols="40" rows="3">{{ isset($account->comment_history) ? $account->comment_history : '' }}</textarea> --}}
    {{-- {{ Form::textarea('comment_history', null ,$attributes = ['class'=>'form-control','rows'=>3]); }} --}}
    @include('fields.textarea', ['name' => 'comment_history', 'attributes' => ['class' => 'form-control', 'rows' => 3]])

</div>

<div class="form-group col-md-4">
    {{-- {{ Form::label('description_summary', 'Description/Summary') }} --}}
    @include('fields.label',['name'=>'Description/Summary'])

    {{-- <textarea name="description_summary" id="" class="form-control" cols="40" rows="3">{{ isset($account->description_summary) ? $account->description_summary : '' }}</textarea> --}}
    {{-- {{ Form::textarea('description_summary', null ,$attributes = ['class'=>'form-control','rows'=>3]); }} --}}
    @include('fields.textarea', ['name' => 'description_summary', 'attributes' => ['class' => 'form-control', 'rows' => 3]])

</div>
</div>





{{-- @foreach ($fields as $field)
                <div class="col">

                    @foreach ($field as $item)
                    {{-- @dump($item) --}}
{{--
                    @include('fields.' . $item['type'], $item)
                    @endforeach
                </div>

            @endforeach --}}
