<div>
    <div class="form-group">
        <label for="{{ $name }}">{{ $label }}</label>
        <input type="text" name="{{ $name }}" id="{{ $name }}" value="{{ $value }}" class="{{ $class }}" :attributes="{{ $attributes }}">
    </div>
</div>

{{-- <div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="text" name="{{ $name }}" value="{{ $value }}" {{ $attributes }}>
</div> --}}




{{-- <div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="text" name="{{ $name }}" value="{{ $value }}" attributes={!! implode(' ', array_map(fn($key, $value) => "$key=\"$value\"", array_keys($attributes), $attributes)) !!}>
</div> --}}
