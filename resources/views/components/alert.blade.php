<div class="alert alert-{{ $type }}">
    <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
    {{ $message }}
</div>
