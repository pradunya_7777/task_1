{{-- <input type="hidden" name="method" value="stores">
<input type="hidden" name="method" value="products"> --}}
@php
if (isset($attributes['table'])) {
    $tableName = $attributes['table'];
    $data = DB::table($tableName)->pluck('name', 'id');
}

// $relatedModule = array([
//             'stores'=>[
//                 'store_id'=>[
//                     $data
//                 ]
//             ],
//             'products'=>[
//                 'product_id'=>[
//                     $data
//                 ]
//             ],
//         ]);
        // @dd($relatedModule);

@endphp

{{-- @dd(in_array('table', $attributes)) --}}

{{ Form::select($name, $data, null, $attributes) }}
