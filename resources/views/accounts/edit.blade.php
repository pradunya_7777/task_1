@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    {{-- <div class="card-header">{{ __('Dashboard') }}</div> --}}

                    <div class="container">
                        <h4 class="mt-3">Account Update</h4>
                        {{-- <form class="mt-2 mb-5" id="update-form" method="post" action="{{ route('accounts.update',['id'=>$account->id ]) }}"> --}}
                            {{  Form::model($account, ['route' => ['accounts.update', $account->id]],['class'=>'mt-2','id'=>'update-form'])  }}
                            {{ Form::token(); }}
                            @include('accounts.form')
                            @method('POST')
                            <div class="mt-4">
                                <a href="#">
                                    <button type="submit" class="btn btn-outline-primary mx-4">Save</button>
                                </a>
                                <a href="{{ route('accounts.view') }}">
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
