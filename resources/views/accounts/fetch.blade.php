@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <div class="card">
                    <div class="container mt-2">
                        <a href="{{ route('accounts.create') }}">Add Accounts</a>
                        <table id="table" >
                            <thead >
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Partner Code</th>
                            </thead>
                            <tbody>
                                @foreach ($accounts as $account)
                                <tr>
                                    <td>{{ $contact->name }}</td>
                                    <td>{{ $account->email }}</td>
                                    <td>{{ $account->phone }}</td>
                                    <td>{{ $account->partnercode }}</td>
                                    <td>{{ $account->industry }}</td>
                                    <td>{{ $account->website }}</td>
                                    <td>{{ $account->comment_history }}</td>
                                    <td>{{ $account->description_summary }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
