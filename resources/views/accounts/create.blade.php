@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    <div class="container h-50">
                        <h4 class="mt-3">Account</h4>
                        {{-- <form class="mt-2 mb-5" method="post" action="{{ route('accounts.store') }}"> --}}
                            {{ Form::open(['route' => ['accounts.store']]) }}
                            {{ Form::token() }}
                            @include('accounts.form')

                            <div class="mt-4 mb-3">

                                {{-- <button type="submit" class="btn btn-outline-primary mx-4">Save</button> --}}
                                {{ Form::submit('Save',['class'=>'btn btn-outline-primary mx-4']) }}
                                <a href={{ route('accounts.view') }}>
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
