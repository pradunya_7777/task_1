@foreach (@config('layouts.account.edit') as $fields)
    {{-- @dump($fields) --}}

    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach
