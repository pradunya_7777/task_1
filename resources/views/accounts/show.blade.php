@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card ">
                    <div class="container mb-3">
                        <h3 class="mt-3">Detail view</h3>

                        <table class="table table-borderless mt-2">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{ $account->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $account->email }}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>{{ $account->phone }}</td>
                                </tr>
                                <tr>
                                    <td>Partner Code</td>
                                    <td>{{ $account->partnercode }}</td>
                                </tr>
                                <tr>
                                    <td>Industry</td>
                                    <td>{{ $account->industry }}</td>
                                </tr>
                                <tr>
                                    <td>website</td>
                                    <td>{{ $account->website }}</td>
                                </tr>
                                <tr>
                                    <td>Comment/History</td>
                                    <td>{{ $account->comment_history }}</td>
                                </tr>
                                <tr>
                                    <td>Description/Summary</td>
                                    <td>{{ $account->description_summary }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('accounts.edit', ['id' => $account->id]) }}">
                            <button class="btn btn-primary mx-2">Edit</button>
                        </a>
                        <a href="{{ route('accounts.view') }}">
                            <button class="btn btn-danger mx-2">Cancel</button>
                        </a>
                        <a href="{{ route('accounts.delete', ['id' => $account->id]) }}">
                            <button class="btn btn-danger mx-2">Del</button>
                        </a>

                        <table class="table table-borderless mt-2">

                            <tbody>
                                <tr>
                                    <td>
                                        <h3>All Contacts</h3>
                                    </td>
                                </tr>
                                @foreach ($contacts as $contact)
                                    <tr>
                                        <td>Name</td>
                                        <td><a
                                                href="{{ route('contacts.show', ['id' => $contact->id]) }}">{{ $contact->name }}</a>
                                        </td>

                                    </tr>
                                @endforeach
                                <tr>
                                    <td>
                                        <h3>All Leads</h3>
                                    </td>
                                </tr>

                                @foreach ($leads as $lead)
                                    <tr>
                                        <td>Name</td>
                                        <td><a
                                                href="{{ route('leads.show', ['lead' => $lead->id]) }}">{{ $lead->name }}</a>
                                        </td>

                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
