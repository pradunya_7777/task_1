@extends('layouts.app')

@section('content')
    <style>
        .table-container {
            width: 100%;
            max-height: 500px;
            /* overflow: auto; */
        }

        th,
        td {
            white-space: nowrap;
        }

        tr {
            height: 50px;
        }
    </style>
    <script src="{{ asset('js/account.js') }}"></script>
    <script src="{{ asset('js/accountupdate.js') }}"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('accounts.create') }}" class="px-3 py-2">Add account</a>
                    <div class="table-container">
                        {{-- <x-alert type='success' message='Welcome to Accounts'/> --}}


                        <table id="table" class="table">
                            <thead class="table-dark">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Partner Code</th>
                                <th>Industry</th>
                                <th>Website</th>
                                <th>Comment/History</th>
                                <th>Desription/Summary</th>

                            </thead>
                            <tbody id="account-list">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
