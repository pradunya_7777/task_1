

@foreach (@config('layouts.student.edit') as $fields)

    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach


{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('student_name', 'Student Name') }} @error('student_name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('student_name', null, ['class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{ Form::label('gender', 'Gender') }}
        <div class="form-check-group">
            {{ Form::radio('gender', 'Male'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('gender', 'Male') }}

            {{ Form::radio('gender', 'Female'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('gender', 'Female') }}


            {{ Form::radio('gender', 'Other'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('gender', 'Other') }}
        </div>
    </div>
</div>

<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('tutions', 'Tution') }}
        {{ Form::select('tutions[]', $tution , null, ['placeholder' => 'Select Tution', 'class' => 'form-select' ,'multiple'=>'multiple']) }}

    </div>
</div>


<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("dob", "Date of birth")}}
        {{ Form::date('dob', null,['class' => 'form-control'] ) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label("contact", "Contact")}}
        {{ Form::number('contact', null , $attributes = ['class' => 'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("guardian_name", "Gurdian Name")}}
        {{ Form::text('guardian_name', null,['class' => 'form-control'] ) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label("guardian_contact", "Gurdian Contact")}}
        {{ Form::number('guardian_contact', null , $attributes = ['class' => 'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">

        {{ Form::label('class', 'Class Name') }}
        {{ Form::select('class', ['Bcom'=>'Bcom','Mcom'=>'Mcom','BA'=>'BA','MA'=>'MA','BSC'=>'BSC','MSC'=>'MSC','BCA'=>'BCA', 'MCA'=>'MCA','BE'=>'BE', 'ME'=>'ME' ,'B-Tec'=>'B-Tec','M-Tec'=>'M-tech'] ,null, ['class' => 'form-select','placeholder'=>'Select Class']) }}

    </div>
    <div class="form-group col-md-4">

        {{ Form::label('year', 'Class Year') }}
        {{ Form::select('year', ['First Year'=>'First Year','Second Year'=>'Second Year','Third Year'=>'Third Year','Forth Year'=>'Forth Year','Fifth Year'=>'Fifth Year','Sixth Year'=>'Sixth Year'] ,null, ['class' => 'form-select','placeholder'=>'Select Year']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">

        {{ Form::label('department', 'Deapartment Name') }}
        {{ Form::select('department', ['Arts'=>'Arts','Commerce'=>'Commerce','Science'=>'Science','Engineering'=>'Engineering','Pharmacy'=>'Pharmacy','Medical'=>'Medical','Other'=>'Other'] ,null, ['class' => 'form-select','placeholder'=>'Select Department']) }}

    </div>
    <div class="form-group col-md-4">

        {{ Form::label('address', 'Address') }}
        {{ Form::textarea('address', null ,$attributes = ['class'=>'form-control','rows'=>3]); }}

    </div>

</div> --}}
