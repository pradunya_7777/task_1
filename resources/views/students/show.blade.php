@extends('layouts.app')

@section('content')
<script src="{{ asset('js/productdetach.js') }}"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container ">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <td>Student Name</td>
                                    <td>{{ $student->name }}</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>{{ $student->gender }}</td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>{{ $student->dob }}</td>
                                </tr>
                                <tr>
                                    <td>Contact</td>
                                    <td>{{ $student->contact }}</td>
                                </tr>
                                <tr>
                                    <td>Gurdian Name</td>
                                    <td>{{ $student->guardian_name }}</td>
                                </tr>
                                <tr>
                                    <td>Gurdian Contact</td>
                                    <td>{{ $student->guardian_contact }}</td>
                                </tr>
                                <tr>
                                    <td>Class</td>
                                    <td>{{ $student->class }}</td>
                                </tr><tr>
                                    <td>Year</td>
                                    <td>{{ $student->year }}</td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td>{{ $student->department }}</td>
                                </tr><tr>
                                    <td>Address</td>
                                    <td>{{ $student->address }}</td>
                                </tr>

                            </tbody>
                        </table>

                            <input type="hidden" id="hidden" name="relation_ship" value="{{ $student->id }}" />
                            <input type="hidden" id="model" name="model" value="Student" />
                            <input type="hidden" id="function_name" name="function_name" value="tutions" />

                            <a href="{{ route('students.edit',['student'=>$student->id]) }}">
                                <button class="btn btn-primary mx-2 mb-3">Edit</button>
                            </a>
                            <a href="{{ route('students.index') }}">
                                <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                            </a>
                            <a>
                            <form action="{{ route('students.destroy', ['student' => $student->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger mx-2 mb-3">Delete</button>
                            </form>
                        </a>
                        <table class="table table-borderless mt-2">

                            <tbody>
                                <tr>
                                    <td>
                                        <h3>All Tutions</h3>
                                    </td>
                                </tr>
                                @foreach ($tutions as $tution)
                                    <tr>
                                        <td>Tution Name</td>
                                        <td>
                                        {{-- <td><a href="{{ route('stores.show', ['store' => $store->id]) }}" class="store-link">{{ $store->name }}</a> --}}
                                            <a href="{{ route('tutions.show', ['tution' => $tution->id]) }}" class="store-link" >{{ $tution->name }}</a>

                                        </td>
                                        <td>

                                        <button class="myButton btn btn-danger" id="myButton" data-child-id="{{ $tution->id }}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                          </svg></button>



                                        </td>
                                    </tr>
                                @endforeach



                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
