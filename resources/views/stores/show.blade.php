@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="container ">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td>Store Name</td>
                                    <td>{{ $store->name }}</td>
                                </tr>
                                <tr>
                                    <td>Store City</td>
                                    <td>{{ $store->city }}</td>
                                </tr>
                                <tr>
                                    <td>Store type</td>
                                    <td>{{ $store->shop_type }}</td>
                                </tr><tr>
                                    <td>Store Service</td>
                                    <td>{{ $store->service }}</td>
                                </tr>
                            </tbody>
                        </table>

                            <a href="{{ route('stores.edit',['store'=>$store->id]) }}">
                                <button class="btn btn-primary mx-2 mb-3">Edit</button>
                            </a>
                            <a href="{{ route('stores.index') }}">
                                <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                            </a>
                            <a>
                            <form action="{{ route('stores.destroy', ['store' => $store->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger mx-2 mb-3">Delete</button>
                            </form>
                        </a>
{{--
                            <a href="{{ route('stores.destroy',['store'=>$store->id]) }}">
                                <button class="btn btn-danger mx-2 mb-3">Delete</button>
                            </a> --}}

                            <table class="table table-borderless mt-2">

                                <tbody>
                                    <tr>
                                        <td><h3>All Products</h3></td>
                                    </tr>
                            @foreach ($products as $product )

                                    <tr>
                                        <td>Product Name</td>
                                        <td>{{ $product->name }}</td>
                                    </tr>

                            @endforeach

                                </tbody>
                            </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
