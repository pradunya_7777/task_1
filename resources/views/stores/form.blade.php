

@foreach (@config('layouts.store.edit') as $fields)

    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach


{{--
<div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('name', 'Store Name') }} @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('name', null, ['class' => 'form-control']) }}

    </div>
    <div class="form-group col-md-4">
        {{  Form::label("products", "Select Products")}}
        {{ Form::select('products[]', $products , null, ['class' => 'form-select', 'multiple'  ]) }}
    </div>
</div>


<div class="row pt-3">

    <div class="form-group col-md-4">
        {{ Form::label("shop_type", "Type of Shop")}}
        {{ Form::select('shop_type', ['WholeSaler' => 'WholeSaler', 'Retailer' => 'Retailer','Fruit Mart'=>'Fruit Mart','Super Shop'=>'Super Shop','Dairy'=>'Dairy','Other'=>'Other'], null , ['placeholder' => 'Select Shop type','class'=>'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label("service", "Service")}}
        {{ Form::select('service', ['Walk-IN' => 'Walk-IN', 'Home Delivery' => 'Home Delivery','Local-Deal'=>'Local-Deal','Other'=>'Other'], null , ['placeholder' => 'Services','class'=>'form-control']) }}

    </div>

</div>

<div class="row pt-3">

    <div class="form-group col-md-4">

        {{ Form::label('city', 'City Name') }}
        {{ Form::text('city', null, ['class' => 'form-control']) }}

        </div>

</div>
 --}}
