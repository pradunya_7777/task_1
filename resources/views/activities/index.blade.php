@extends('layouts.app')

@section('content')
<style>
    .table-container {
            width: 100%;
            max-height: 500px;
            /* overflow: auto; */
        }
        th, td { white-space: nowrap;}
        tr { height: 50px;}
</style>
    <script src="{{ asset('js/activity.js') }}"></script>
    <script src="{{ asset('js/activityupdate.js') }}"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <a href="{{ route('activities.create') }} "class="px-3 py-3">Add Activity</a>
                    <div class="table-container">
                        <table id="table" class="table">

                            <thead class="table-dark">
                                <th>Subject</th>
                                <th>Related TO</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Task Type</th>
                                <th>Status</th>
                                <th >Description</th>

                            </thead>
                            <tbody id="account-list">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
