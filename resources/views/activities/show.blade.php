@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    <div class="container ">

                        <h3>Detail view</h3>
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <td>Subject</td>
                                    <td>{{ $activity->subject }}</td>
                                </tr>
                                <tr>
                                    <td>Relater to</td>
                                    <td>{{ $activity->relatedTo }}</td>
                                </tr>
                                <tr>
                                    <td>Start Date</td>
                                    <td>{{ $activity->start }}</td>
                                </tr>
                                <tr>
                                    <td>End Date</td>
                                    <td>{{ $activity->end }}</td>
                                </tr>
                                <tr>
                                    <td>Task Type</td>
                                    <td>{{ $activity->taskType }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{{ $activity->status }}</td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>{{ $activity->description }}</td>
                                </tr>

                            </tbody>
                        </table>
                        <a href="{{ route('activities.edit',['activity'=>$activity->id]) }}">
                            <button class="btn btn-primary mx-2 mb-3">Edit</button>
                        </a>
                        <a href="{{ route('activities.index') }}">
                            <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                        </a>
                        <form action="{{ route('activities.destroy',['activity'=>$activity->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger mx-2 mb-3">Delete</button>
                        </form>

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
