@foreach (@config('layouts.activity.edit') as $fields)
    <div class="row">

        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach



{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('subject', 'Subject') }}@error('subject')
        <div class="text-danger">{{ $message }}</div>
    @enderror
    {{ Form::text('subject', null, ['class' => 'form-control']) }}
    <x-text-input name="name" label="Name" value=""  :attributes=' ["class"=>"form-control", "placeholder"=>"Enter Name", "Selected", "id"=>"Name"] ' />


</div>

<div class="form-group col-md-4">
    {{ Form::label('relatedTo', 'Related to') }}
    {{ Form::select('relatedTo', ['Contact' => 'Contact', 'Account' => 'Account', 'Project' => 'Project'], null, ['placeholder' => 'select relate To', 'class' => 'form-control']) }}

</div>
<div class="form-group col-md-3">
    {{ Form::label('project_id', 'Project') }}
    {{ Form::select('project_id', $project, null, ['placeholder' => 'Select Account', 'class' => 'form-select']) }}
</div>
</div>
<div class="row pt-3">
<div class="form-group col-md-4">
    {{ Form::label('start', 'Start date') }}@error('start')
    <div class="text-danger">{{ $message }}</div>
@enderror
{{ Form::dateTimeLocal('start', null, ['class' => 'form-control']) }}
</div>

<div class="form-group col-md-4">
{{ Form::label('end', 'End Date') }}
{{ Form::dateTimeLocal('end', null, ['class' => 'form-control']) }}

</div>
</div>
<div class="row pt-3">
<div class="form-group col-md-4">
{{ Form::label('taskType', 'Task type') }}
{{ Form::select('taskType', ['Coading' => 'Coading', 'Demo/Training' => 'Demo/Training', 'Documentation' => 'Documentation', 'Machine Installation' => 'Machine Installation', 'Other' => 'Other'], null, ['placeholder' => 'Type of task', 'class' => 'form-control']) }}

</div>

<div class="form-group col-md-6">
<div class="form-group col-md-8">
    {{ Form::label('status', 'Status') }}
    <div class="form-check-group">

        {{ Form::radio('status', 'Assign') }}
        {{ Form::label('status', 'Assign'), null, ['class' => 'form-check-input'] }}

        {{ Form::radio('status', 'Inprocess') }}
        {{ Form::label('status', 'Inprocess'), null, ['class' => 'form-check-input'] }}

        {{ Form::radio('status', 'Completed') }}
        {{ Form::label('status', 'Completed'), null, ['class' => 'form-check-input'] }}

    </div>


</div>
</div>
<div class="row pt-3">

<div class="form-group col-md-4">
    {{ Form::label('description', 'Description') }}
    {{ Form::textarea('description', null, $attributes = ['class' => 'form-control', 'rows' => 3]) }}

</div>

</div> --}}
