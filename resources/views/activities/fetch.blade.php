@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="container">
                        <a href="{{ route('activities.create') }}">Add Activity</a>
                        <table class="table-striped">
                            <thead class="table-dark">
                                <th>Subject</th>
                                <th>RelatedTo</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Task Type</th>
                                <th>Status</th>
                                <th>Description</th>

                            </thead>
                            <tbody>
                                @foreach ($activities as $activity)
                                <tr>
                                    <td>
                                        <a href="">{{ $activity->name }}</a>
                                    </td>
                                    <td>{{ $activity->realatedTo }}</td>
                                    <td>{{ $activity->start }}</td>
                                    <td>{{ $activity->end }}</td>
                                    <td>{{ $activity->taskType }}</td>
                                    <td>{{ $activity->status }}</td>
                                    <td>{{ $activity->description }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
