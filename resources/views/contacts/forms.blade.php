@foreach (@config('layouts.contact.edit') as $fields)

    <div class="row">


        @foreach ($fields as $field)
            <div class="col pt-3">
                @foreach ($field as $item)
                    @include('fields.' . $item['type'], $item)
                @endforeach
            </div>
        @endforeach

    </div>
@endforeach




{{-- <div class="row pt-3">
    <div class="form-group col-md-4">

        {{ Form::label('name', 'Name') }} @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::text('name', null, ['class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4 ">
        {{ Form::label('designation', 'Designation') }}
        {{ Form::text('designation',null , ['class' => 'form-control']) }}


    </div>
</div>

<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('account_id', 'Account') }}
        {{ Form::select('account_id', $account, null, ['placeholder' => 'Select Account', 'class' => 'form-control']) }}
    </div>
</div>

<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', null, ['class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', null, ['class' => 'form-control']) }}

    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('customer_type', 'Customer Type') }}
        {{ Form::select('customer_type', ['IT Dealer' => 'IT Dealer', 'Customer' => 'Customer', 'Competitor' => 'Competitor', 'Currior Company' => 'Currior Company', 'Not Know' => 'Not Know'], null, ['placeholder' => 'Select Customer type', 'class' => 'form-control']) }}

    </div>

    <div class="form-group col-md-4">
        {{ Form::label('customer_position', 'Customer Position') }} @error('customer_position')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        {{ Form::select('customer_position', ['Sales head' => 'Sales head', 'Junior' => 'Junior', 'Senior' => 'Senior', 'Manager' => 'Manager', 'Owner' => 'Owner'], null, ['placeholder' => 'Select Customer type', 'class' => 'form-control']) }}

    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', null, $attributes = ['class' => 'form-control', 'rows' => 3]) }}
    </div>

    <div class="form-group col-md-4">
        {{ Form::label('comment', 'Comment') }}
        {{ Form::textarea('comment', null, $attributes = ['class' => 'form-control', 'rows' => 3]) }}
    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::label('status', 'Contact Status') }}
        <div class="form-check-group">
            {{ Form::radio('status', 'Inprocess'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'Contacted') }}

            {{ Form::radio('status', 'Inprocess'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'Not Yet') }}


            {{ Form::radio('status', 'Inprocess'), null, ['class' => 'form-check-input'] }}
            {{ Form::label('contact_status', 'Rejected') }}
        </div>
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('followupdate', 'Follow-up date') }}
        {{ Form::dateTimeLocal('followupdate', null, ['class' => 'form-control']) }}

    </div>
</div>
<div class="row pt-3">
    <div class="form-group col-md-4">
        {{ Form::checkbox('joincommunity', 'yes'), null, ['class' => 'form-check-input'] }}
        {{ Form::label('joincommunity', 'Joined Community ?') }}
    </div>
</div> --}}
