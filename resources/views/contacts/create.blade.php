@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 h-100">
                <div class="card">
                    <div class="container">
                        <h4 class="mt-3">Contacts</h4>
                        {{ Form::open(['route' => ['contacts.store']]) }}
                        {{ Form::token() }}
                            @include('contacts.forms')

                            <div class="mt-4 mb-2">

                                <button type="submit" class="btn btn-outline-primary mx-5">Save</button>
                                <a href='{{ route('contacts.view') }}'>
                                    <button type="button" class="btn btn-danger">Cancel</button>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
