@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    {{-- @include('accounts.accountheader') --}}
                    <div class="container ">

                        <h4 class="mt-3">Detail view</h4>
                        <table class="table table-borderless">
                            <tbody>

                                <i class="fas fa-trash"></i>
                                <tr>
                                    <td>Name</td>
                                    <td>{{ $contact->name }}</td>
                                </tr>
                                <tr>
                                    <td>Designation</td>
                                    <td>{{ $contact->designation }}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>{{ $contact->phone }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $contact->email }}</td>
                                </tr>
                                <tr>
                                    <td>Customer type</td>
                                    <td>{{ $contact->customer_type }}</td>
                                </tr>
                                <tr>
                                    <td>Customer Position</td>
                                    <td>{{ $contact->customer_position }}</td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>{{ $contact->description }}</td>
                                </tr>
                                <tr>
                                    <td>Comment</td>
                                    <td>{{ $contact->comment }}</td>
                                </tr>
                                <tr>
                                    <td>Contact Status</td>
                                    <td>{{ $contact->contact_status }}</td>
                                </tr>
                                <tr>
                                    <td>Join Community</td>
                                    <td>{{ $contact->joincommunity }}</td>
                                </tr>
                                <tr>
                                    <td>Follow Up Date & Time</td>
                                    <td>{{ $contact->followupdate }}</td>
                                </tr>

                            </tbody>
                        </table>



                            <a href="{{ route('contacts.edit', ['id' => $contact->id]) }}">
                                <button class="btn btn-primary mx-2 mb-3">Edit</button>
                            </a>
                            <a href="{{ route('contacts.view') }}">
                                <button class="btn btn-secondary mx-2 mb-3">Cancel</button>
                            </a>
                            <a href="{{ route('contacts.delete', ['id' => $contact->id]) }}">
                                <button class="btn btn-danger mx-2 mb-3">Del</button>
                            </a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
