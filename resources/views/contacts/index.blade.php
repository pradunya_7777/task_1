@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/contact.js') }}"></script>
    <script src="{{ asset('js/contactupdate.js') }}"></script>
    <style>
        .table-container {
            width: 100%;
            max-height: 500px;
            /* overflow: auto; */
        }
        th, td { white-space: nowrap;}
        tr { height: 50px;}


    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('contacts.create') }}" class="px-3 py-3">Add Contacts</a>
                    <div class="table-container">
                        <table id="table" class="table hover">
                            <thead class="table-dark">
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Customer type</th>
                                <th>Customer Position</th>
                                <th>Description</th>
                                <th>Comment</th>
                                <th>Contact status</th>
                                <th>Follw up date</th>
                                <th>Join Community</th>
                                <th>Account Name</th>


                            </thead>
                            <tbody id="contact-list">

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
