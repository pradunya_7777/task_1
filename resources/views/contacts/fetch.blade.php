@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="container"style="width: 100%; overflow-x: auto;">
                        <a href="{{ route('contacts.create') }}">Add Contacts</a>
                        <table class="table" >
                            <thead class="table-dark">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Customer Type</th>
                                <th>Customer Position</th>
                                <th>Description</th>
                                <th>Comment</th>
                                <th>Contact Status</th>

                            </thead>
                            <tbody>
                                @foreach ($contacts as $contact)
                                <tr>
                                    <td>
                                        <a href="">{{ $contact->name }}</a>
                                    </td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->phone }}</td>
                                    <td>{{ $contact->customer_type }}</td>
                                    <td>{{ $contact->customer_position }}</td>
                                    <td>{{ $contact->description }}</td>
                                    <td>{{ $contact->customer_type }}</td>
                                    <td>{{ $contact->customer_type }}</td>
                                    <td>{{ $contact->accounts_name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
